<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="connector">
<packages>
<package name="UCBUS_MODULE">
<smd name="P$1" x="3.175" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$2" x="4.445" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$3" x="5.715" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<wire x1="0" y1="0" x2="28.575" y2="0" width="0.127" layer="51"/>
<wire x1="28.575" y1="0" x2="28.575" y2="20.32" width="0.127" layer="51"/>
<wire x1="1.27" y1="20.32" x2="0" y2="19.05" width="0.127" layer="51"/>
<wire x1="0" y1="19.05" x2="0" y2="0" width="0.127" layer="51"/>
<smd name="P$4" x="6.985" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$5" x="8.255" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$6" x="9.525" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$7" x="10.795" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$8" x="12.065" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$9" x="13.335" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$10" x="15.875" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$11" x="17.145" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$12" x="18.415" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$13" x="19.685" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$14" x="20.955" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$15" x="22.225" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$16" x="23.495" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$17" x="24.765" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$18" x="26.035" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$19" x="28.7274" y="1.27" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$20" x="28.7274" y="2.54" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$21" x="28.7274" y="3.81" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$22" x="28.7274" y="5.08" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$23" x="28.7274" y="6.35" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$24" x="28.7274" y="7.62" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$25" x="28.7274" y="8.89" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$26" x="28.7274" y="10.16" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$27" x="13.335" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$28" x="12.065" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$29" x="10.795" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$30" x="9.525" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$31" x="8.255" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$32" x="6.985" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$33" x="5.715" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$34" x="4.445" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$35" x="3.175" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<wire x1="18.796" y1="20.32" x2="19.177" y2="19.939" width="0.127" layer="51"/>
<wire x1="19.177" y1="19.939" x2="26.924" y2="19.939" width="0.127" layer="51"/>
<wire x1="26.924" y1="19.939" x2="27.305" y2="20.32" width="0.127" layer="51"/>
<wire x1="27.305" y1="20.32" x2="28.575" y2="20.32" width="0.127" layer="51"/>
<wire x1="18.796" y1="20.32" x2="1.27" y2="20.32" width="0.127" layer="51"/>
<wire x1="0.127" y1="19.05" x2="1.27" y2="20.193" width="0.127" layer="21"/>
<wire x1="28.448" y1="20.193" x2="28.067" y2="20.193" width="0.127" layer="21"/>
<wire x1="28.448" y1="20.193" x2="28.448" y2="19.812" width="0.127" layer="21"/>
<wire x1="28.448" y1="0.127" x2="28.448" y2="0.508" width="0.127" layer="21"/>
<wire x1="28.448" y1="0.127" x2="28.067" y2="0.127" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.127" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.508" y2="0.127" width="0.127" layer="21"/>
</package>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
<package name="SOLDER_ON">
<smd name="P$1" x="0" y="0" dx="4" dy="7" layer="1" thermals="no"/>
</package>
<package name="SOLDER_JUMPER">
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="UCBUS_MODULE">
<pin name="GND" x="-12.7" y="-35.56" length="middle"/>
<pin name="+3V3" x="-12.7" y="-27.94" length="middle"/>
<pin name="+5V" x="-12.7" y="-20.32" length="middle"/>
<pin name="PA02/DAC-0/ADC0-1" x="68.58" y="27.94" length="middle" rot="R180"/>
<pin name="PA03/VREFA/ADC0-1" x="68.58" y="25.4" length="middle" rot="R180"/>
<pin name="PA04/VREFB/ACD0-4/TC0-0/SER0-0" x="68.58" y="22.86" length="middle" rot="R180"/>
<pin name="PA05/DAC-1/ADC0-5/TC0-1/SER0-1" x="68.58" y="20.32" length="middle" rot="R180"/>
<pin name="PA06/VREFC/ADC0-6/TC1-0/SER0-2" x="68.58" y="17.78" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/TC1-1/SER0-3" x="68.58" y="15.24" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1" x="68.58" y="12.7" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/TC0-1/TCC0-1/SER0-1/SER2-0" x="68.58" y="10.16" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/TC1-0/TCC0-2/SER0-2/SER2-2" x="68.58" y="7.62" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/TC1-1/TCC0-3/SER0-3/SER2-3" x="68.58" y="5.08" length="middle" rot="R180"/>
<pin name="PA12/TC2-0/TCC0-6/SER2-0/SER4-1" x="68.58" y="2.54" length="middle" rot="R180"/>
<pin name="PA13/TC2-1/TCC0-7/SER2-1/SER4-0" x="68.58" y="0" length="middle" rot="R180"/>
<pin name="PA20/TC7-0/SER5-2/SER3-2" x="68.58" y="-5.08" length="middle" rot="R180"/>
<pin name="PA21/TC7-1/SER5-3/SER3-3" x="68.58" y="-7.62" length="middle" rot="R180"/>
<pin name="PA22/TC4-0/SER5-1/SER3-0" x="68.58" y="-10.16" length="middle" rot="R180"/>
<pin name="PA23/TC4-1/SER5-0/SER3-1" x="68.58" y="-12.7" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="68.58" y="-17.78" length="middle" rot="R180"/>
<pin name="PB07/AD1-9" x="68.58" y="-20.32" length="middle" rot="R180"/>
<pin name="PB10/TC5-0/TCC0-4/SER4-2" x="68.58" y="-22.86" length="middle" rot="R180"/>
<pin name="PB11/TC5-1/TCC0-5/SER4-3" x="68.58" y="-25.4" length="middle" rot="R180"/>
<pin name="PB12/TC4-0/SER4-0" x="68.58" y="-27.94" length="middle" rot="R180"/>
<pin name="PB13/TC4-1/SER4-1" x="68.58" y="-30.48" length="middle" rot="R180"/>
<pin name="PB14/TC5-0/SER4-2" x="68.58" y="-33.02" length="middle" rot="R180"/>
<pin name="PB15/TC5-1/SER4-3" x="68.58" y="-35.56" length="middle" rot="R180"/>
<pin name="A" x="-12.7" y="27.94" length="middle"/>
<pin name="B" x="-12.7" y="25.4" length="middle"/>
<pin name="Y" x="-12.7" y="20.32" length="middle"/>
<pin name="Z" x="-12.7" y="17.78" length="middle"/>
<pin name="GND_PA04-PA02" x="-12.7" y="-10.16" length="middle"/>
<pin name="GND_PB10-PA20" x="-12.7" y="-7.62" length="middle"/>
<pin name="GND_PA11-PB14" x="-12.7" y="-5.08" length="middle"/>
<wire x1="-7.62" y1="30.48" x2="-7.62" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-38.1" x2="63.5" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-38.1" x2="63.5" y2="30.48" width="0.4064" layer="94"/>
<wire x1="63.5" y1="30.48" x2="-7.62" y2="30.48" width="0.4064" layer="94"/>
</symbol>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="SOLDER_JUMPER">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UCBUS_MODULE" prefix="U">
<gates>
<gate name="G$1" symbol="UCBUS_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UCBUS_MODULE">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$25"/>
<connect gate="G$1" pin="+5V" pad="P$26"/>
<connect gate="G$1" pin="A" pad="P$19"/>
<connect gate="G$1" pin="B" pad="P$20"/>
<connect gate="G$1" pin="GND" pad="P$21 P$22"/>
<connect gate="G$1" pin="GND_PA04-PA02" pad="P$31"/>
<connect gate="G$1" pin="GND_PA11-PB14" pad="P$5"/>
<connect gate="G$1" pin="GND_PB10-PA20" pad="P$14"/>
<connect gate="G$1" pin="PA02/DAC-0/ADC0-1" pad="P$32"/>
<connect gate="G$1" pin="PA03/VREFA/ADC0-1" pad="P$33"/>
<connect gate="G$1" pin="PA04/VREFB/ACD0-4/TC0-0/SER0-0" pad="P$30"/>
<connect gate="G$1" pin="PA05/DAC-1/ADC0-5/TC0-1/SER0-1" pad="P$29"/>
<connect gate="G$1" pin="PA06/VREFC/ADC0-6/TC1-0/SER0-2" pad="P$28"/>
<connect gate="G$1" pin="PA07/ADC0-7/TC1-1/SER0-3" pad="P$27"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1" pad="P$1"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/TC0-1/TCC0-1/SER0-1/SER2-0" pad="P$2"/>
<connect gate="G$1" pin="PA10/ADC0-10/TC1-0/TCC0-2/SER0-2/SER2-2" pad="P$3"/>
<connect gate="G$1" pin="PA11/ADC0-11/TC1-1/TCC0-3/SER0-3/SER2-3" pad="P$4"/>
<connect gate="G$1" pin="PA12/TC2-0/TCC0-6/SER2-0/SER4-1" pad="P$8"/>
<connect gate="G$1" pin="PA13/TC2-1/TCC0-7/SER2-1/SER4-0" pad="P$9"/>
<connect gate="G$1" pin="PA20/TC7-0/SER5-2/SER3-2" pad="P$15"/>
<connect gate="G$1" pin="PA21/TC7-1/SER5-3/SER3-3" pad="P$16"/>
<connect gate="G$1" pin="PA22/TC4-0/SER5-1/SER3-0" pad="P$17"/>
<connect gate="G$1" pin="PA23/TC4-1/SER5-0/SER3-1" pad="P$18"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="P$34"/>
<connect gate="G$1" pin="PB07/AD1-9" pad="P$35"/>
<connect gate="G$1" pin="PB10/TC5-0/TCC0-4/SER4-2" pad="P$13"/>
<connect gate="G$1" pin="PB11/TC5-1/TCC0-5/SER4-3" pad="P$12"/>
<connect gate="G$1" pin="PB12/TC4-0/SER4-0" pad="P$11"/>
<connect gate="G$1" pin="PB13/TC4-1/SER4-1" pad="P$10"/>
<connect gate="G$1" pin="PB14/TC5-0/SER4-2" pad="P$6"/>
<connect gate="G$1" pin="PB15/TC5-1/SER4-3" pad="P$7"/>
<connect gate="G$1" pin="Y" pad="P$23"/>
<connect gate="G$1" pin="Z" pad="P$24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLDER" package="SOLDER_ON">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="SOLDER_JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOLDER_JUMPER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X15-SHROUDED">
<wire x1="-2.775" y1="18.415" x2="-2.775" y2="17.145" width="0.2032" layer="21"/>
<wire x1="4.5" y1="22.8" x2="4.5" y2="-22.8" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-22.8" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="22.8" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="22.8" x2="4.4" y2="22.8" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-22.8" x2="-4.5" y2="-22.8" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="21.7" x2="3.4" y2="21.7" width="0.2032" layer="51"/>
<wire x1="3.4" y1="21.7" x2="3.4" y2="-21.7" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-21.7" x2="3.4" y2="-21.7" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="21.7" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-21.7" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="17.78" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="17.78" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="15.24" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="15.24" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="12.7" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="12.7" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="10.16" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="10.16" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="7.62" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="7.62" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<text x="-2.921" y="20.574" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<text x="-2.921" y="-21.082" size="0.4064" layer="104">&gt;NAME</text>
<rectangle x1="-1.524" y1="17.526" x2="-1.016" y2="18.034" layer="51"/>
<rectangle x1="1.016" y1="17.526" x2="1.524" y2="18.034" layer="51"/>
<rectangle x1="1.016" y1="14.986" x2="1.524" y2="15.494" layer="51"/>
<rectangle x1="-1.524" y1="14.986" x2="-1.016" y2="15.494" layer="51"/>
<rectangle x1="1.016" y1="12.446" x2="1.524" y2="12.954" layer="51"/>
<rectangle x1="-1.524" y1="12.446" x2="-1.016" y2="12.954" layer="51"/>
<rectangle x1="1.016" y1="9.906" x2="1.524" y2="10.414" layer="51"/>
<rectangle x1="-1.524" y1="9.906" x2="-1.016" y2="10.414" layer="51"/>
<rectangle x1="1.016" y1="7.366" x2="1.524" y2="7.874" layer="51"/>
<rectangle x1="-1.524" y1="7.366" x2="-1.016" y2="7.874" layer="51"/>
<rectangle x1="-1.524" y1="9.906" x2="-1.016" y2="10.414" layer="51"/>
<rectangle x1="1.016" y1="9.906" x2="1.524" y2="10.414" layer="51"/>
<pad name="11" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="12" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="13" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="14" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="15" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="16" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="17" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="18" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="19" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="20" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<pad name="21" x="-1.27" y="-7.62" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="22" x="1.27" y="-7.62" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="23" x="-1.27" y="-10.16" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="24" x="1.27" y="-10.16" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="25" x="-1.27" y="-12.7" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="26" x="1.27" y="-12.7" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="1.016" y1="-7.874" x2="1.524" y2="-7.366" layer="51"/>
<rectangle x1="-1.524" y1="-7.874" x2="-1.016" y2="-7.366" layer="51"/>
<rectangle x1="1.016" y1="-10.414" x2="1.524" y2="-9.906" layer="51"/>
<rectangle x1="-1.524" y1="-10.414" x2="-1.016" y2="-9.906" layer="51"/>
<rectangle x1="1.016" y1="-12.954" x2="1.524" y2="-12.446" layer="51"/>
<rectangle x1="-1.524" y1="-12.954" x2="-1.016" y2="-12.446" layer="51"/>
<rectangle x1="-1.524" y1="-10.414" x2="-1.016" y2="-9.906" layer="51"/>
<rectangle x1="1.016" y1="-10.414" x2="1.524" y2="-9.906" layer="51"/>
<pad name="27" x="-1.27" y="-15.24" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="28" x="1.27" y="-15.24" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="29" x="-1.27" y="-17.78" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="30" x="1.27" y="-17.78" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
</package>
<package name="2X5">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
</package>
<package name="2X5-RA">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Male Headers&lt;/h3&gt;
tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.8" y1="6.3" x2="5.3" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="6.3" x2="5.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-6.3" x2="2.8" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6.3" x2="2.8" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="0" x2="11.3" y2="0" width="0.127" layer="51"/>
<wire x1="5.3" y1="-2.54" x2="11.3" y2="-2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="-5.08" x2="11.3" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.3" y1="2.54" x2="11.3" y2="2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="5.08" x2="11.3" y2="5.08" width="0.127" layer="51"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="13.8" y1="6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="6.3" x2="13.8" y2="6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="-6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<text x="2.54" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.54" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RAF">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Female Header&lt;/h3&gt;
Silk outline of pins
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.7" y1="6.3" x2="11.2" y2="6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="6.3" x2="11.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-6.3" x2="2.7" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-6.3" x2="2.7" y2="6.3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="3.175" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.175" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.318" y="10.414" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="10.16" x2="-3.175" y2="10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="10.16" x2="4.445" y2="10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="10.16" x2="4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-10.16" x2="-3.175" y2="-10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-10.16" x2="4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-10.16" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="4" x="2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="6" x="2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="8" x="2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="10" x="2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5_NOSILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<text x="-0.889" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-2.159" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_PTH_SILK_.05">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
Holes are 0.05". 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.4318" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.4318" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.4318" rot="R90"/>
<pad name="4" x="1.27" y="1.27" drill="0.4318" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="0.4318" rot="R90"/>
<pad name="6" x="2.54" y="1.27" drill="0.4318" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.4318" rot="R90"/>
<pad name="8" x="3.81" y="1.27" drill="0.4318" rot="R90"/>
<pad name="9" x="5.08" y="0" drill="0.4318" rot="R90"/>
<pad name="10" x="5.08" y="1.27" drill="0.4318" rot="R90"/>
<wire x1="-0.635" y1="0.635" x2="-0.762" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="1.778" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.778" x2="-0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.635" y2="1.905" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.762" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.032" y2="2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.032" x2="3.048" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.032" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.302" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.302" y1="2.032" x2="4.318" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.318" y1="2.032" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.572" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.572" y1="2.032" x2="5.588" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.032" x2="5.842" y2="1.778" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.762" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="0.635" x2="5.842" y2="0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="-0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.588" y1="-0.762" x2="4.572" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.572" y1="-0.762" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.318" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.318" y1="-0.762" x2="3.302" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.048" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.508" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="22"/>
<text x="-0.762" y="2.286" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED-NS">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="1X02">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_POKEHOME">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-7.239" y="-1.397" size="0.3048" layer="51" rot="R90">BOARD EDGE</text>
</package>
<package name="1X02_RA_PTH_FEMALE">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
</packages>
<symbols>
<symbol name="CONN_15X2">
<description>&lt;h3&gt; 26 Pin Connection&lt;/h3&gt;
13x2 pin layout</description>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-20.32" x2="3.81" y2="20.32" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="2.54" y2="12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="15.24" x2="2.54" y2="15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="17.78" x2="2.54" y2="17.78" width="0.6096" layer="94"/>
<text x="-3.81" y="-22.606" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="20.828" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="26" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="24" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="22" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="20" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="18" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="16" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="14" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-3.81" y1="20.32" x2="3.81" y2="20.32" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="12.7" x2="-2.54" y2="12.7" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="15.24" x2="-2.54" y2="15.24" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="17.78" x2="-2.54" y2="17.78" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="20.32" x2="-3.81" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-20.32" x2="-3.81" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="-2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-10.16" x2="-2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-12.7" x2="-2.54" y2="-12.7" width="0.6096" layer="94"/>
<pin name="1" x="-7.62" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="11" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="13" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="15" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="17" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="19" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="21" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="23" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="25" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="27" x="-7.62" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="28" x="7.62" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="29" x="-7.62" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="30" x="7.62" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-1.27" y1="-15.24" x2="-2.54" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-17.78" x2="-2.54" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="2.54" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-17.78" x2="2.54" y2="-17.78" width="0.6096" layer="94"/>
</symbol>
<symbol name="CONN_05X2">
<description>&lt;h3&gt;10 Pin Connection&lt;/h3&gt;
5x2 pin layout</description>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CONN_02">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_15X2" prefix="J">
<gates>
<gate name="G$1" symbol="CONN_15X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X15-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_05X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;h3&gt;For AVR SPI programming port, see special device with nice symbol: "AVR_SPI_PROG_5x2.dev"&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/778"&gt; 2x5 AVR ICSP Male Header&lt;/a&gt; (PRT-00778)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8506"&gt; 2x5 Pin Shrouded Header&lt;/a&gt; (PRT-08506)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 5x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8535"&gt; 2x5 Pin IDC Ribbon Cable&lt;/a&gt; (PRT-08535)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_05X2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08499" constant="no"/>
<attribute name="SF_ID" value="PRT-0778" constant="no"/>
</technology>
</technologies>
</device>
<device name="RA" package="2X5-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAF" package="2X5-RAF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD" package="2X5-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK" package="2X5-SHROUDED_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_SMD" package="2X5-SHROUDED_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09508" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X5_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05_IN_PTH_SILK" package="2X5_PTH_SILK_.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD-NS" package="2X5-SHROUDED-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="BFC-UM">
<pad name="P$1" x="-4.9784" y="0" drill="2.032" diameter="3.81"/>
<pad name="P$2" x="4.9784" y="0" drill="2.032" diameter="3.81"/>
<circle x="0" y="0" radius="9.9822" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BFC-8MM">
<pad name="P$1" x="-2" y="0" drill="1" diameter="1.9304"/>
<pad name="P$2" x="2" y="0" drill="1" diameter="1.9304"/>
<circle x="0" y="0" radius="6" width="0.127" layer="21"/>
<text x="-1" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="BFC-18MM-7-5MM">
<pad name="P$1" x="-3.75" y="0" drill="1" diameter="1.6764" shape="square" thermals="no"/>
<pad name="P$2" x="3.75" y="0" drill="1" diameter="1.778" thermals="no"/>
<circle x="0" y="0" radius="9" width="0.127" layer="51"/>
<wire x1="-2.5" y1="0" x2="-1.75" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="0" x2="-1.5" y2="0" width="0.127" layer="21"/>
<wire x1="2.5" y1="0" x2="1.5" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0.5" x2="2" y2="-0.5" width="0.127" layer="21"/>
</package>
<package name="R-78BX-2.0">
<pad name="P$2" x="0" y="0" drill="0.9" diameter="1.6764" shape="square"/>
<pad name="P$1" x="-2.54" y="0" drill="0.9" diameter="1.6764"/>
<pad name="P$3" x="2.54" y="0" drill="0.9" diameter="1.6764"/>
<wire x1="-5.75" y1="2" x2="-5.75" y2="-6.5" width="0.0762" layer="51"/>
<wire x1="-5.75" y1="-6.5" x2="5.75" y2="-6.5" width="0.0762" layer="51"/>
<wire x1="5.75" y1="-6.5" x2="5.75" y2="2" width="0.0762" layer="51"/>
<wire x1="5.75" y1="2" x2="-5.75" y2="2" width="0.0762" layer="51"/>
<text x="0" y="-2" size="0.8128" layer="51" font="vector" align="center">17.5mm tall</text>
<wire x1="-5.75" y1="2" x2="-5.75" y2="1.75" width="0.1524" layer="21"/>
<wire x1="-5.75" y1="2" x2="-5.5" y2="2" width="0.1524" layer="21"/>
<wire x1="5.5" y1="2" x2="5.75" y2="2" width="0.1524" layer="21"/>
<wire x1="5.75" y1="2" x2="5.75" y2="1.75" width="0.1524" layer="21"/>
<wire x1="5.75" y1="-6.25" x2="5.75" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="5.75" y1="-6.5" x2="5.5" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="-5.75" y1="-6.25" x2="-5.75" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="-5.75" y1="-6.5" x2="-5.5" y2="-6.5" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1" y1="0" x2="1.8542" y2="2.4892" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="1.8504" y1="-2.4668" x2="1.0161" y2="0" width="0.254" layer="94" curve="-37.373024"/>
<text x="-1.27" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="REGULATOR">
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="-1.27" x2="6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="point"/>
<pin name="GND" x="0" y="-2.54" length="point" rot="R90"/>
<pin name="OUT" x="7.62" y="2.54" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BFC" prefix="C">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BFC-UM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8MM" package="BFC-8MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BFC-18MM" package="BFC-18MM-7-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-78BX-2.0">
<gates>
<gate name="G$1" symbol="REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R-78BX-2.0">
<connects>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="IN" pad="P$1"/>
<connect gate="G$1" pin="OUT" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="0805">
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.6" x2="1" y2="0.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
<text x="0" y="1.8" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMA-403D">
<smd name="P$1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.2" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2" y2="1.6" width="0.127" layer="21"/>
<text x="0" y="2.4" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA403D" package="SMA-403D">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="raspberrypi_bastelstube_v13">
<description>&lt;b&gt;Raspberry Pi&lt;/b&gt; by &lt;b&gt;Martin's Bastelstube&lt;/b&gt;
&lt;p&gt;
Different shapes of piggyback boards for the famous &amp;amp; fabulous
&lt;a href="http://www.raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt; computer.
&lt;p&gt;
The outline of the Raspberry Pi board is drawn on layer 52 bDocu, so you can make it invisible if necessary.&lt;br/&gt;
Recommendation: Start your design with RASPI_BOARD_*_FULL, so you are aware of all the big components and
connectors when you're drawing the dimensions of your board and placing your own components.&lt;br/&gt;
Later, you can simply change the package of your Raspberry Pi connector to the RASPI_BOARD_*_EDGES type,
so you can concentrate on your own design.
&lt;p&gt;
Most of my board packages don't have the outline drawn in layer 20 Dimension (except the HATs). This allows you to deviate from the original Raspberry Pi board shape, e.g. when you need more space behind the GPIO connector for the routing of the power supply lines. If you want to stick exactly to the Raspberry Pi board shape, then just draw the Dimension layer right on the lines in layer bDocu.

&lt;hr&gt;&lt;br/&gt;

Version 1.3
&lt;ul&gt;
&lt;li&gt;Added the shape and dimensions of the HAT board with &amp;amp; without the camera and display flex cutout&lt;/li&gt;
&lt;li&gt;New GPIO connector version (suffix &amp;quot;#&amp;quot;) with all pins connected (no more &amp;quot;n.c.&amp;quot; pins)&lt;/li&gt;
&lt;li&gt;Two more &amp;quot;AB-&amp;quot; boards with 5 and 6 drill holes / pads, mechanically compatible with all RasPi versions&lt;/li&gt;
&lt;/ul&gt;

Version 1.2
&lt;ul&gt;
&lt;li&gt;Added a 26-pin GPIO connector with board dimensions of model B+&lt;br/&gt;
       for backwards compatibility with models A and B, named &amp;quot;B-&amp;quot;&lt;/li&gt;
&lt;/ul&gt;

Version 1.1
&lt;ul&gt;
&lt;li&gt;New GPIO connector and board dimensions of model B+&lt;/li&gt;
&lt;li&gt;Old connector and board (for models A and B) renamed to AB&lt;/li&gt;
&lt;li&gt;GPIO pin numbers on connector changed to match Rev2 board design&lt;/li&gt;
&lt;/ul&gt;

Version 1.0
&lt;ul&gt;
&lt;li&gt;More detailed descriptions for all components&lt;/li&gt;
&lt;li&gt;Published on Cadsoft webpage&lt;/li&gt;
&lt;/ul&gt;

&lt;hr&gt;&lt;br/&gt;

Designed by &lt;a href="http://bastelstube.rocci.net/"&gt;Martin's Bastelstube&lt;/a&gt;&lt;br&gt;
Comments / complaints / bug reports / suggestions to &lt;author&gt;eightbit@web.de&lt;/author&gt;</description>
<packages>
<package name="RASPI_BOARD_B+_FULL">
<description>Raspberry Pi board model B+, full outline with position of big connectors &amp;amp; drill holes</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="0" x2="6.85" y2="0" width="0.254" layer="52"/>
<wire x1="6.85" y1="0" x2="14.35" y2="0" width="0.254" layer="52"/>
<wire x1="14.35" y1="0" x2="50.5" y2="0" width="0.254" layer="52"/>
<wire x1="50.5" y1="0" x2="51.5" y2="0" width="0.254" layer="52"/>
<wire x1="51.5" y1="0" x2="55.5" y2="0" width="0.254" layer="52"/>
<wire x1="55.5" y1="0" x2="56.5" y2="0" width="0.254" layer="52"/>
<wire x1="56.5" y1="0" x2="82" y2="0" width="0.254" layer="52"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="3" x2="85" y2="53" width="0.254" layer="52"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="56" x2="3" y2="56" width="0.254" layer="52"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<wire x1="0" y1="53" x2="0" y2="3" width="0.254" layer="52"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<wire x1="65.25" y1="2.25" x2="86.5" y2="2.25" width="0.127" layer="52"/>
<wire x1="86.5" y1="2.25" x2="86.5" y2="18.25" width="0.127" layer="52"/>
<wire x1="86.5" y1="18.25" x2="65.25" y2="18.25" width="0.127" layer="52"/>
<wire x1="65.25" y1="18.25" x2="65.25" y2="2.25" width="0.127" layer="52"/>
<wire x1="69" y1="35.6" x2="69" y2="22.4" width="0.127" layer="52"/>
<wire x1="69" y1="22.4" x2="86" y2="22.4" width="0.127" layer="52"/>
<wire x1="86" y1="22.4" x2="86" y2="21.7" width="0.127" layer="52"/>
<wire x1="86" y1="21.7" x2="86.5" y2="21.7" width="0.127" layer="52"/>
<wire x1="86.5" y1="21.7" x2="86.5" y2="36.3" width="0.127" layer="52"/>
<wire x1="86.5" y1="36.3" x2="86" y2="36.3" width="0.127" layer="52"/>
<wire x1="86" y1="36.3" x2="86" y2="35.6" width="0.127" layer="52"/>
<wire x1="86" y1="35.6" x2="69" y2="35.6" width="0.127" layer="52"/>
<wire x1="69" y1="40.4" x2="86" y2="40.4" width="0.127" layer="52"/>
<wire x1="86" y1="40.4" x2="86" y2="39.7" width="0.127" layer="52"/>
<wire x1="86" y1="39.7" x2="86.5" y2="39.7" width="0.127" layer="52"/>
<wire x1="86.5" y1="39.7" x2="86.5" y2="54.3" width="0.127" layer="52"/>
<wire x1="86.5" y1="54.3" x2="86" y2="54.3" width="0.127" layer="52"/>
<wire x1="86" y1="54.3" x2="86" y2="53.6" width="0.127" layer="52"/>
<wire x1="86" y1="53.6" x2="69" y2="53.6" width="0.127" layer="52"/>
<wire x1="69" y1="53.6" x2="69" y2="40.4" width="0.127" layer="52"/>
<wire x1="24.5" y1="10.5" x2="24.5" y2="-1.25" width="0.127" layer="52"/>
<wire x1="24.5" y1="-1.25" x2="39.5" y2="-1.25" width="0.127" layer="52"/>
<wire x1="39.5" y1="-1.25" x2="39.5" y2="10.5" width="0.127" layer="52"/>
<wire x1="39.5" y1="10.5" x2="24.5" y2="10.5" width="0.127" layer="52"/>
<wire x1="50.5" y1="0" x2="50.5" y2="11" width="0.127" layer="52"/>
<wire x1="50.5" y1="11" x2="56.5" y2="11" width="0.127" layer="52"/>
<wire x1="56.5" y1="11" x2="56.5" y2="0" width="0.127" layer="52"/>
<wire x1="51.5" y1="0" x2="51.5" y2="-2" width="0.127" layer="52"/>
<wire x1="51.5" y1="-2" x2="55.5" y2="-2" width="0.127" layer="52"/>
<wire x1="55.5" y1="-2" x2="55.5" y2="0" width="0.127" layer="52"/>
<wire x1="6.85" y1="0" x2="6.85" y2="5.75" width="0.127" layer="52"/>
<wire x1="6.85" y1="5.75" x2="14.35" y2="5.75" width="0.127" layer="52"/>
<wire x1="14.35" y1="5.75" x2="14.35" y2="0" width="0.127" layer="52"/>
<wire x1="6.85" y1="0" x2="6.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="6.6" y1="-0.5" x2="14.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="14.6" y1="-0.5" x2="14.35" y2="0" width="0.127" layer="52"/>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.1" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.1" width="0.127" layer="52"/>
</package>
<package name="RASPI_BOARD_B+_FULL_PAD">
<description>Raspberry Pi board model B+, full outline with position of big connectors &amp;amp; mounting pads</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="0" x2="6.85" y2="0" width="0.254" layer="52"/>
<wire x1="6.85" y1="0" x2="14.35" y2="0" width="0.254" layer="52"/>
<wire x1="14.35" y1="0" x2="50.5" y2="0" width="0.254" layer="52"/>
<wire x1="50.5" y1="0" x2="51.5" y2="0" width="0.254" layer="52"/>
<wire x1="51.5" y1="0" x2="55.5" y2="0" width="0.254" layer="52"/>
<wire x1="55.5" y1="0" x2="56.5" y2="0" width="0.254" layer="52"/>
<wire x1="56.5" y1="0" x2="82" y2="0" width="0.254" layer="52"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="3" x2="85" y2="53" width="0.254" layer="52"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="56" x2="3" y2="56" width="0.254" layer="52"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<wire x1="0" y1="53" x2="0" y2="3" width="0.254" layer="52"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<wire x1="65.25" y1="2.25" x2="86.5" y2="2.25" width="0.127" layer="52"/>
<wire x1="86.5" y1="2.25" x2="86.5" y2="18.25" width="0.127" layer="52"/>
<wire x1="86.5" y1="18.25" x2="65.25" y2="18.25" width="0.127" layer="52"/>
<wire x1="65.25" y1="18.25" x2="65.25" y2="2.25" width="0.127" layer="52"/>
<wire x1="69" y1="35.6" x2="69" y2="22.4" width="0.127" layer="52"/>
<wire x1="69" y1="22.4" x2="86" y2="22.4" width="0.127" layer="52"/>
<wire x1="86" y1="22.4" x2="86" y2="21.7" width="0.127" layer="52"/>
<wire x1="86" y1="21.7" x2="86.5" y2="21.7" width="0.127" layer="52"/>
<wire x1="86.5" y1="21.7" x2="86.5" y2="36.3" width="0.127" layer="52"/>
<wire x1="86.5" y1="36.3" x2="86" y2="36.3" width="0.127" layer="52"/>
<wire x1="86" y1="36.3" x2="86" y2="35.6" width="0.127" layer="52"/>
<wire x1="86" y1="35.6" x2="69" y2="35.6" width="0.127" layer="52"/>
<wire x1="69" y1="40.4" x2="86" y2="40.4" width="0.127" layer="52"/>
<wire x1="86" y1="40.4" x2="86" y2="39.7" width="0.127" layer="52"/>
<wire x1="86" y1="39.7" x2="86.5" y2="39.7" width="0.127" layer="52"/>
<wire x1="86.5" y1="39.7" x2="86.5" y2="54.3" width="0.127" layer="52"/>
<wire x1="86.5" y1="54.3" x2="86" y2="54.3" width="0.127" layer="52"/>
<wire x1="86" y1="54.3" x2="86" y2="53.6" width="0.127" layer="52"/>
<wire x1="86" y1="53.6" x2="69" y2="53.6" width="0.127" layer="52"/>
<wire x1="69" y1="53.6" x2="69" y2="40.4" width="0.127" layer="52"/>
<wire x1="24.5" y1="10.5" x2="24.5" y2="-1.25" width="0.127" layer="52"/>
<wire x1="24.5" y1="-1.25" x2="39.5" y2="-1.25" width="0.127" layer="52"/>
<wire x1="39.5" y1="-1.25" x2="39.5" y2="10.5" width="0.127" layer="52"/>
<wire x1="39.5" y1="10.5" x2="24.5" y2="10.5" width="0.127" layer="52"/>
<wire x1="50.5" y1="0" x2="50.5" y2="11" width="0.127" layer="52"/>
<wire x1="50.5" y1="11" x2="56.5" y2="11" width="0.127" layer="52"/>
<wire x1="56.5" y1="11" x2="56.5" y2="0" width="0.127" layer="52"/>
<wire x1="51.5" y1="0" x2="51.5" y2="-2" width="0.127" layer="52"/>
<wire x1="51.5" y1="-2" x2="55.5" y2="-2" width="0.127" layer="52"/>
<wire x1="55.5" y1="-2" x2="55.5" y2="0" width="0.127" layer="52"/>
<wire x1="6.85" y1="0" x2="6.85" y2="5.75" width="0.127" layer="52"/>
<wire x1="6.85" y1="5.75" x2="14.35" y2="5.75" width="0.127" layer="52"/>
<wire x1="14.35" y1="5.75" x2="14.35" y2="0" width="0.127" layer="52"/>
<wire x1="6.85" y1="0" x2="6.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="6.6" y1="-0.5" x2="14.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="14.6" y1="-0.5" x2="14.35" y2="0" width="0.127" layer="52"/>
<pad name="DRILL_3" x="3.5" y="3.5" drill="2.75" diameter="6.2"/>
<pad name="DRILL_1" x="3.5" y="52.5" drill="2.75" diameter="6.2"/>
<pad name="DRILL_2" x="61.5" y="52.5" drill="2.75" diameter="6.2"/>
<pad name="DRILL_4" x="61.5" y="3.5" drill="2.75" diameter="6.2"/>
</package>
<package name="RASPI_BOARD_B+_EDGES_0DRILL">
<description>Raspberry Pi board model B+, outline is only marked by its edges&lt;br&gt;
without any drill holes</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
</package>
<package name="RASPI_BOARD_B+_EDGES_4DRILL">
<description>Raspberry Pi board model B+, outline is only marked by its edges&lt;br/&gt;
with all 4 drill holes</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="52.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.1" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.1" width="0.127" layer="52"/>
</package>
<package name="RASPI_BOARD_B+_EDGES_4DRILL_PAD">
<description>Raspberry Pi board model B+, outline is only marked by its edges&lt;br/&gt;
with all 4 mounting pads</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<pad name="41" x="3.5" y="3.5" drill="2.75" diameter="6.2"/>
<pad name="42" x="61.5" y="3.5" drill="2.75" diameter="6.2"/>
<pad name="43" x="61.5" y="52.5" drill="2.75" diameter="6.2"/>
<pad name="44" x="3.5" y="52.5" drill="2.75" diameter="6.2"/>
</package>
<package name="RASPI_BOARD_B+_EDGES_3DRILL">
<description>Raspberry Pi board model B+, outline is only marked by its edges&lt;br&gt;
with 3 drill holes (top left hole is missing to allow routing of power supply lines)</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.1" width="0.127" layer="52"/>
</package>
<package name="RASPI_BOARD_B+_EDGES_3DRILL_PAD">
<description>Raspberry Pi board model B+, outline is only marked by its edges&lt;br&gt;
with 3 mounting pads (top left pad is missing to allow routing of power supply lines)</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.254" layer="52" curve="90"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<pad name="41" x="3.5" y="3.5" drill="2.75" diameter="6.2"/>
<pad name="42" x="61.5" y="3.5" drill="2.75" diameter="6.2"/>
<pad name="43" x="61.5" y="52.5" drill="2.75" diameter="6.2"/>
</package>
<package name="RASPI_BOARD_B+HAT">
<description>Raspberry Pi board model B+ HAT board&lt;br/&gt;
Official outline, but without cutouts for display &amp;amp; camera flex connector</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0" layer="20" curve="90"/>
<wire x1="3" y1="0" x2="62" y2="0" width="0" layer="20"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0" layer="20" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="53" width="0" layer="20"/>
<wire x1="65" y1="53" x2="62" y2="56" width="0" layer="20" curve="90"/>
<wire x1="62" y1="56" x2="3" y2="56" width="0" layer="20"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0" layer="20" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="52.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<circle x="3.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="30"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="30"/>
<wire x1="0" y1="3" x2="0" y2="53" width="0" layer="20"/>
</package>
<package name="RASPI_BOARD_B+HAT_DF">
<description>Raspberry Pi board model B+ HAT board&lt;br/&gt;
Official outline with cutout for display flex connector</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0" layer="20" curve="90"/>
<wire x1="3" y1="0" x2="62" y2="0" width="0" layer="20"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0" layer="20" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="53" width="0" layer="20"/>
<wire x1="65" y1="53" x2="62" y2="56" width="0" layer="20" curve="90"/>
<wire x1="62" y1="56" x2="3" y2="56" width="0" layer="20"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0" layer="20" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="52.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<circle x="3.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="30"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="30"/>
<wire x1="0" y1="3" x2="0" y2="17.5" width="0" layer="20"/>
<wire x1="0" y1="17.5" x2="2" y2="19.5" width="0" layer="20" curve="-90"/>
<wire x1="2" y1="19.5" x2="3" y2="19.5" width="0" layer="20"/>
<wire x1="3" y1="19.5" x2="5" y2="21.5" width="0" layer="20" curve="90"/>
<wire x1="5" y1="21.5" x2="5" y2="34.5" width="0" layer="20"/>
<wire x1="5" y1="34.5" x2="3" y2="36.5" width="0" layer="20" curve="90"/>
<wire x1="3" y1="36.5" x2="2" y2="36.5" width="0" layer="20"/>
<wire x1="2" y1="36.5" x2="0" y2="38.5" width="0" layer="20" curve="-90"/>
<wire x1="0" y1="38.5" x2="0" y2="53" width="0" layer="20"/>
<text x="4" y="21" size="1.016" layer="51" font="vector" ratio="10" rot="R90">Display Flex Slot</text>
</package>
<package name="RASPI_BOARD_B+HAT_DF_CF">
<description>Raspberry Pi board model B+ HAT board&lt;br/&gt;
Official outline with cutouts for display &amp;amp; camera flex connector</description>
<wire x1="0" y1="3" x2="3" y2="0" width="0" layer="20" curve="90"/>
<wire x1="3" y1="0" x2="62" y2="0" width="0" layer="20"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0" layer="20" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="53" width="0" layer="20"/>
<wire x1="65" y1="53" x2="62" y2="56" width="0" layer="20" curve="90"/>
<wire x1="62" y1="56" x2="3" y2="56" width="0" layer="20"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0" layer="20" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="52.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<pad name="1" x="8.37" y="51.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="53.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="51.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="53.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="51.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="53.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="51.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="53.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="51.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="53.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="51.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="53.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="51.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="53.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="51.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="53.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="51.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="53.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="51.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="53.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="51.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="53.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="51.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="53.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="51.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="53.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="51.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="53.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="51.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="53.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="51.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="53.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="51.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="53.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="51.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="53.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="51.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="53.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="51.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="53.77" drill="1" diameter="1.778"/>
<wire x1="7.1" y1="55.04" x2="7.1" y2="52.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="7.1" y2="49.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="49.96" x2="8.0525" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.96" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="49.96" x2="57.9" y2="49.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="49.96" x2="57.9" y2="55.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="55.04" x2="7.1" y2="55.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="52.5" x2="9.64" y2="52.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="52.5" x2="9.64" y2="49.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.96" x2="8.0525" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="49.6425" x2="8.6875" y2="49.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="49.6425" x2="8.6875" y2="49.96" width="0.127" layer="21"/>
<text x="8.0525" y="48.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<circle x="3.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.2" width="0.127" layer="52"/>
<circle x="61.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0.127" layer="52"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="29"/>
<circle x="3.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="52.5" radius="3.2" width="0" layer="30"/>
<circle x="61.5" y="3.5" radius="3.2" width="0" layer="30"/>
<circle x="3.5" y="3.5" radius="3.2" width="0" layer="30"/>
<wire x1="0" y1="3" x2="0" y2="17.5" width="0" layer="20"/>
<wire x1="0" y1="17.5" x2="2" y2="19.5" width="0" layer="20" curve="-90"/>
<wire x1="2" y1="19.5" x2="3" y2="19.5" width="0" layer="20"/>
<wire x1="3" y1="19.5" x2="5" y2="21.5" width="0" layer="20" curve="90"/>
<wire x1="5" y1="21.5" x2="5" y2="34.5" width="0" layer="20"/>
<wire x1="5" y1="34.5" x2="3" y2="36.5" width="0" layer="20" curve="90"/>
<wire x1="3" y1="36.5" x2="2" y2="36.5" width="0" layer="20"/>
<wire x1="2" y1="36.5" x2="0" y2="38.5" width="0" layer="20" curve="-90"/>
<wire x1="0" y1="38.5" x2="0" y2="53" width="0" layer="20"/>
<wire x1="44" y1="4" x2="45" y2="3" width="0" layer="20" curve="90"/>
<wire x1="45" y1="3" x2="46" y2="4" width="0" layer="20" curve="90"/>
<wire x1="46" y1="4" x2="46" y2="19" width="0" layer="20"/>
<wire x1="46" y1="19" x2="45" y2="20" width="0" layer="20" curve="90"/>
<wire x1="45" y1="20" x2="44" y2="19" width="0" layer="20" curve="90"/>
<wire x1="44" y1="19" x2="44" y2="4" width="0" layer="20"/>
<text x="4" y="21" size="1.016" layer="51" font="vector" ratio="10" rot="R90">Display Flex Slot</text>
<text x="45.5" y="4.75" size="1.016" layer="51" font="vector" ratio="10" rot="R90">Camera Flex Slot</text>
</package>
</packages>
<symbols>
<symbol name="RASPI_GPIO_B+#">
<description>Generic schematic symbol for the Raspberry Pi GPIO connector&lt;br/&gt;
Model B+ &amp;nbsp; all pins connected</description>
<wire x1="-12.7" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="-38.1" x2="-12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="13.97" y="22.86" size="1.778" layer="97" ratio="10">Raspberry Pi</text>
<text x="13.97" y="20.32" size="1.778" layer="97" ratio="10">GPIO Port</text>
<pin name="5V0A" x="-7.62" y="27.94" length="short" direction="pwr" rot="R270"/>
<pin name="3V3A" x="5.08" y="27.94" length="short" direction="pwr" rot="R270"/>
<pin name="GND_A" x="15.24" y="-7.62" length="short" direction="pwr" rot="R180"/>
<pin name="SDA" x="-15.24" y="-35.56" length="short"/>
<pin name="SCL" x="-15.24" y="-33.02" length="short"/>
<pin name="GPIO17" x="-15.24" y="15.24" length="short"/>
<pin name="GPIO18" x="-15.24" y="12.7" length="short"/>
<pin name="GPIO27" x="-15.24" y="10.16" length="short"/>
<pin name="GPIO22" x="-15.24" y="7.62" length="short"/>
<pin name="GPIO23" x="-15.24" y="5.08" length="short"/>
<pin name="GPIO24" x="-15.24" y="2.54" length="short"/>
<pin name="GPIO25" x="-15.24" y="0" length="short"/>
<pin name="GPIO04" x="-15.24" y="-2.54" length="short"/>
<pin name="TXD" x="15.24" y="0" length="short" rot="R180"/>
<pin name="RXD" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="SPI_MOSI" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="SPI_MISO" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="SPI_SCLK" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="SPI_CE0" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="SPI_CE1" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="5V0B" x="-5.08" y="27.94" length="short" direction="pwr" rot="R270"/>
<pin name="3V3B" x="7.62" y="27.94" length="short" direction="pwr" rot="R270"/>
<pin name="GND_B" x="15.24" y="-10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND_C" x="15.24" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GND_D" x="15.24" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="GND_E" x="15.24" y="-17.78" length="short" direction="pwr" rot="R180"/>
<text x="13.97" y="17.78" size="1.778" layer="97" ratio="10">Model B+</text>
<pin name="ID_SC" x="15.24" y="-33.02" length="short" direction="nc" rot="R180"/>
<pin name="ID_SD" x="15.24" y="-35.56" length="short" direction="nc" rot="R180"/>
<pin name="GPIO05" x="-15.24" y="-7.62" length="short" direction="nc"/>
<pin name="GPIO06" x="-15.24" y="-10.16" length="short" direction="nc"/>
<pin name="GPIO12" x="-15.24" y="-12.7" length="short" direction="nc"/>
<pin name="GPIO13" x="-15.24" y="-15.24" length="short" direction="nc"/>
<pin name="GPIO19" x="-15.24" y="-17.78" length="short" direction="nc"/>
<pin name="GPIO16" x="-15.24" y="-20.32" length="short" direction="nc"/>
<pin name="GPIO26" x="-15.24" y="-22.86" length="short" direction="nc"/>
<pin name="GPIO20" x="-15.24" y="-25.4" length="short" direction="nc"/>
<pin name="GPIO21" x="-15.24" y="-27.94" length="short" direction="nc"/>
<pin name="GND_F" x="15.24" y="-20.32" length="short" direction="pwr" rot="R180"/>
<pin name="GND_G" x="15.24" y="-22.86" length="short" direction="pwr" rot="R180"/>
<pin name="GND_H" x="15.24" y="-25.4" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RASPI_BOARD_B+#" prefix="X">
<description>Raspberry Pi GPIO connector with markings for the outer dimensions of the Raspberry Pi board model B+&lt;br&gt;
with different sets of drill holes or mounting pads&lt;br/&gt;
All pins connected</description>
<gates>
<gate name="G$1" symbol="RASPI_GPIO_B+#" x="0" y="0"/>
</gates>
<devices>
<device name="_F" package="RASPI_BOARD_B+_FULL">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_FP" package="RASPI_BOARD_B+_FULL_PAD">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_E0" package="RASPI_BOARD_B+_EDGES_0DRILL">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_E3" package="RASPI_BOARD_B+_EDGES_3DRILL">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_E3P" package="RASPI_BOARD_B+_EDGES_3DRILL_PAD">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_E4" package="RASPI_BOARD_B+_EDGES_4DRILL">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_E4P" package="RASPI_BOARD_B+_EDGES_4DRILL_PAD">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_H" package="RASPI_BOARD_B+HAT">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HD" package="RASPI_BOARD_B+HAT_DF">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HDC" package="RASPI_BOARD_B+HAT_DF_CF">
<connects>
<connect gate="G$1" pin="3V3A" pad="1"/>
<connect gate="G$1" pin="3V3B" pad="17"/>
<connect gate="G$1" pin="5V0A" pad="2"/>
<connect gate="G$1" pin="5V0B" pad="4"/>
<connect gate="G$1" pin="GND_A" pad="6"/>
<connect gate="G$1" pin="GND_B" pad="9"/>
<connect gate="G$1" pin="GND_C" pad="14"/>
<connect gate="G$1" pin="GND_D" pad="20"/>
<connect gate="G$1" pin="GND_E" pad="25"/>
<connect gate="G$1" pin="GND_F" pad="30"/>
<connect gate="G$1" pin="GND_G" pad="34"/>
<connect gate="G$1" pin="GND_H" pad="39"/>
<connect gate="G$1" pin="GPIO04" pad="7"/>
<connect gate="G$1" pin="GPIO05" pad="29"/>
<connect gate="G$1" pin="GPIO06" pad="31"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_CE0" pad="24"/>
<connect gate="G$1" pin="SPI_CE1" pad="26"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="I2C_MODULE_0.91INCH_PLANAR">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.25" y1="-1.75" x2="9.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="9.75" y1="-1.75" x2="9.75" y2="36.25" width="0.127" layer="51"/>
<wire x1="9.75" y1="36.25" x2="-2.25" y2="36.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.75" x2="-2.25" y2="36.25" width="0.127" layer="51"/>
</package>
<package name="I2C_MODULE_0.91INCH_SIDE">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="1" x="7.62" y="0" drill="1.016" diameter="1.8796" shape="offset" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.25" y1="-4.29" x2="9.75" y2="-4.29" width="0.127" layer="51"/>
<wire x1="9.75" y1="-4.29" x2="9.75" y2="-1.85" width="0.127" layer="51"/>
<wire x1="9.75" y1="-1.85" x2="-2.25" y2="-1.85" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-4.29" x2="-2.25" y2="-1.85" width="0.127" layer="51"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" diameter="1.8796" shape="offset" rot="R90"/>
</package>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED-5630">
<smd name="P$2" x="2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$3" x="2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$4" x="-2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$5" x="-2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<rectangle x1="-2.5" y1="-1.5" x2="2.5" y2="1.5" layer="51"/>
<wire x1="0" y1="1.4" x2="0" y2="1.9" width="0.1016" layer="21"/>
<wire x1="0" y1="-1.4" x2="0" y2="-1.9" width="0.1016" layer="21"/>
<polygon width="0" layer="1">
<vertex x="-2.4" y="1"/>
<vertex x="-2.2" y="1"/>
<vertex x="-2" y="1.2"/>
<vertex x="-1.4" y="1.2"/>
<vertex x="-1.4" y="-1.2"/>
<vertex x="-2" y="-1.2"/>
<vertex x="-2.2" y="-1"/>
<vertex x="-2.4" y="-1"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="2.4" y="1"/>
<vertex x="2.2" y="1"/>
<vertex x="2" y="1.2"/>
<vertex x="-0.8" y="1.2"/>
<vertex x="-0.8" y="-1.2"/>
<vertex x="2" y="-1.2"/>
<vertex x="2.2" y="-1"/>
<vertex x="2.4" y="-1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="2.2" y="1.1"/>
<vertex x="2" y="1.3"/>
<vertex x="-0.9" y="1.3"/>
<vertex x="-0.9" y="-1.3"/>
<vertex x="2" y="-1.3"/>
<vertex x="2.2" y="-1.1"/>
<vertex x="3" y="-1.1"/>
<vertex x="3" y="-0.1"/>
<vertex x="2.4" y="-0.1"/>
<vertex x="2.4" y="0.1"/>
<vertex x="3" y="0.1"/>
<vertex x="3" y="1.1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-3" y="1.1"/>
<vertex x="-2.2" y="1.1"/>
<vertex x="-2" y="1.3"/>
<vertex x="-1.3" y="1.3"/>
<vertex x="-1.3" y="-1.3"/>
<vertex x="-2" y="-1.3"/>
<vertex x="-2.2" y="-1.1"/>
<vertex x="-3" y="-1.1"/>
<vertex x="-3" y="-0.1"/>
<vertex x="-2.5" y="-0.1"/>
<vertex x="-2.5" y="0.1"/>
<vertex x="-3" y="0.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-2" y="1.1"/>
<vertex x="-2" y="-1.1"/>
<vertex x="-1.5" y="-1.1"/>
<vertex x="-1.5" y="1.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-0.7" y="1.1"/>
<vertex x="2" y="1.1"/>
<vertex x="2" y="-1.1"/>
<vertex x="-0.7" y="-1.1"/>
</polygon>
<wire x1="2.6" y1="-1.2" x2="2.8" y2="-1.4" width="0.1016" layer="21"/>
<wire x1="2.8" y1="-1.4" x2="2.6" y2="-1.6" width="0.1016" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="2.6" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.9" y1="-1.2" x2="2.9" y2="-1.6" width="0.1016" layer="21"/>
</package>
<package name="0402-D">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="I2C_MODULE">
<pin name="SDA" x="-7.62" y="2.54" length="middle"/>
<pin name="SCL" x="-7.62" y="-2.54" length="middle"/>
<pin name="VCC" x="-7.62" y="-7.62" length="middle"/>
<pin name="GND" x="-7.62" y="-12.7" length="middle"/>
<wire x1="-2.54" y1="5.08" x2="58.42" y2="5.08" width="0.254" layer="94"/>
<wire x1="58.42" y1="5.08" x2="58.42" y2="-15.24" width="0.254" layer="94"/>
<wire x1="58.42" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="I2C_OLED_MODULE" prefix="J">
<gates>
<gate name="G$1" symbol="I2C_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="I2C_MODULE_0.91INCH_PLANAR">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="I2C_MODULE_0.91INCH_SIDE">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5630" package="LED-5630">
<connects>
<connect gate="G$1" pin="A" pad="P$4 P$5"/>
<connect gate="G$1" pin="C" pad="P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-D">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="SOP65P637X110-14N">
<rectangle x1="-0.901" y1="-0.9328" x2="0.901" y2="0.9328" layer="31"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.55" x2="-2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-3.865" y1="2.8" x2="3.865" y2="2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="-2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="2.8" x2="-3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="3.865" y1="2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<text x="-3.73" y="-2.727" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-3.73" y="2.727" size="1.27" layer="25">&gt;NAME</text>
<smd name="1" x="-2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="2" x="-2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="3" x="-2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="4" x="-2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="5" x="-2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="6" x="-2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="7" x="-2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="8" x="2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="9" x="2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="10" x="2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="11" x="2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="12" x="2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="13" x="2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="14" x="2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="15" x="0" y="0" dx="2.85" dy="2.95" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="MAX13450EAUDT">
<wire x1="-12.7" y1="35.56" x2="12.7" y2="35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="-12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="-12.7" y1="-35.56" x2="-12.7" y2="35.56" width="0.41" layer="94"/>
<text x="-12.7" y="36.56" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-12.7" y="-39.56" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="DI" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="!RE" x="-17.78" y="20.32" length="middle"/>
<pin name="!TERM" x="-17.78" y="15.24" length="middle"/>
<pin name="A" x="-17.78" y="10.16" length="middle"/>
<pin name="B" x="-17.78" y="5.08" length="middle"/>
<pin name="DE" x="-17.78" y="0" length="middle"/>
<pin name="RO" x="-17.78" y="-5.08" length="middle"/>
<pin name="SRL" x="-17.78" y="-10.16" length="middle"/>
<pin name="TERM100" x="-17.78" y="-15.24" length="middle"/>
<pin name="Y" x="-17.78" y="-20.32" length="middle"/>
<pin name="Z" x="-17.78" y="-25.4" length="middle"/>
<pin name="VCC" x="17.78" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VL" x="17.78" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="EPAD" x="17.78" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-33.02" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAX13450EAUDT" prefix="U">
<gates>
<gate name="A" symbol="MAX13450EAUDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P637X110-14N">
<connects>
<connect gate="A" pin="!RE" pad="2"/>
<connect gate="A" pin="!TERM" pad="5"/>
<connect gate="A" pin="A" pad="8"/>
<connect gate="A" pin="B" pad="9"/>
<connect gate="A" pin="DE" pad="1"/>
<connect gate="A" pin="DI" pad="3"/>
<connect gate="A" pin="EPAD" pad="15"/>
<connect gate="A" pin="GND" pad="11"/>
<connect gate="A" pin="RO" pad="7"/>
<connect gate="A" pin="SRL" pad="14"/>
<connect gate="A" pin="TERM100" pad="13"/>
<connect gate="A" pin="VCC" pad="4"/>
<connect gate="A" pin="VL" pad="6"/>
<connect gate="A" pin="Y" pad="10"/>
<connect gate="A" pin="Z" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" 1/1 Transceiver Full RS422, RS485 14-TSSOP-EP "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="MAX13450EAUD+T-ND"/>
<attribute name="DIGIKEY-PURCHASE-URL" value="https://snapeda.com/shop?store=DigiKey&amp;id=694117"/>
<attribute name="MF" value="Maxim Integrated"/>
<attribute name="MP" value="MAX13450EAUD+"/>
<attribute name="PACKAGE" value="TSSOP-14 Maxim"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="connector" deviceset="UCBUS_MODULE" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_15X2" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="P+5" library="supply1" deviceset="+24V" device=""/>
<part name="P+6" library="supply1" deviceset="+24V" device=""/>
<part name="C2" library="power" deviceset="BFC" device="BFC-18MM"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+24V" device=""/>
<part name="J4" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J5" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J6" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J7" library="connector" deviceset="PWRPAD" device="SOLDER"/>
<part name="J8" library="connector" deviceset="PWRPAD" device="SOLDER"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="+24V" device=""/>
<part name="C1" library="power" deviceset="BFC" device="BFC-18MM"/>
<part name="C3" library="passives" deviceset="CAP" device="0805" value="100pF"/>
<part name="C4" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0805-RES" value="spare"/>
<part name="X1" library="raspberrypi_bastelstube_v13" deviceset="RASPI_BOARD_B+#" device="_F" value="RASPI_BOARD_B+#_F"/>
<part name="J1" library="lights" deviceset="I2C_OLED_MODULE" device=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_05X2" device="SHD_LOCK"/>
<part name="U$1" library="power" deviceset="R-78BX-2.0" device=""/>
<part name="R3" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="P+7" library="supply1" deviceset="+24V" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="R5" library="passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="J9" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="J10" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="J11" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="J12" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+24V" device=""/>
<part name="P+9" library="supply1" deviceset="+24V" device=""/>
<part name="J13" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="P+10" library="supply1" deviceset="+5V" device=""/>
<part name="U2" library="comm" deviceset="MAX13450EAUDT" device=""/>
<part name="C5" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="P+11" library="supply1" deviceset="+5V" device=""/>
<part name="P+12" library="supply1" deviceset="+24V" device=""/>
<part name="C6" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="P+13" library="supply1" deviceset="+5V" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="J14" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="R6" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="R7" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="P+14" library="supply1" deviceset="+5V" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="C7" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C8" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="C9" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C10" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="R8" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="J15" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="R9" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="J16" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="R10" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="J17" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="R11" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="J18" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="D6" library="passives" deviceset="DIODE" device="SMA403D"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="114.3" y="101.6" smashed="yes"/>
<instance part="P+1" gate="1" x="76.2" y="81.28" smashed="yes" rot="R90">
<attribute name="VALUE" x="73.66" y="81.28" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V1" gate="G$1" x="88.9" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="86.36" y="73.66" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="96.52" y="60.96" smashed="yes">
<attribute name="VALUE" x="93.98" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="55.88" y="157.48" smashed="yes">
<attribute name="VALUE" x="52.07" y="134.874" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="52.07" y="178.308" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND2" gate="1" x="73.66" y="157.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="76.2" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="38.1" y="157.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="35.56" y="160.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+5" gate="1" x="73.66" y="152.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="76.2" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="P+6" gate="1" x="38.1" y="162.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="35.56" y="162.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="50.8" y="106.68" smashed="yes">
<attribute name="NAME" x="49.53" y="109.855" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="60.96" y="48.26" smashed="yes">
<attribute name="VALUE" x="58.42" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="45.72" y="129.54" smashed="yes">
<attribute name="VALUE" x="45.72" y="132.08" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J4" gate="G$1" x="111.76" y="55.88" smashed="yes"/>
<instance part="J5" gate="G$1" x="111.76" y="50.8" smashed="yes"/>
<instance part="J6" gate="G$1" x="111.76" y="45.72" smashed="yes"/>
<instance part="J7" gate="G$1" x="147.32" y="55.88" smashed="yes"/>
<instance part="J8" gate="G$1" x="147.32" y="45.72" smashed="yes"/>
<instance part="GND7" gate="1" x="134.62" y="55.88" smashed="yes" rot="R270">
<attribute name="VALUE" x="132.08" y="58.42" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+8" gate="1" x="134.62" y="45.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="132.08" y="45.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="50.8" y="116.84" smashed="yes">
<attribute name="NAME" x="49.53" y="120.015" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="50.8" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="53.721" y="92.456" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="48.641" y="92.456" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="46.736" y="92.456" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="44.958" y="92.456" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="43.18" y="92.456" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="C4" gate="G$1" x="50.8" y="81.28" smashed="yes" rot="R270">
<attribute name="NAME" x="53.721" y="79.756" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="48.641" y="79.756" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="46.736" y="79.756" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="44.958" y="79.756" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="43.18" y="79.756" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="R1" gate="G$1" x="50.8" y="68.58" smashed="yes">
<attribute name="NAME" x="46.99" y="70.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="65.278" size="1.778" layer="96"/>
<attribute name="PRECISION" x="46.99" y="61.722" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="46.99" y="63.5" size="1.27" layer="97"/>
</instance>
<instance part="R2" gate="G$1" x="50.8" y="58.42" smashed="yes">
<attribute name="NAME" x="46.99" y="59.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="55.118" size="1.778" layer="96"/>
<attribute name="PRECISION" x="46.99" y="51.562" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="46.99" y="53.34" size="1.27" layer="97"/>
</instance>
<instance part="X1" gate="G$1" x="259.08" y="193.04" smashed="yes"/>
<instance part="J1" gate="G$1" x="251.46" y="142.24" smashed="yes"/>
<instance part="J3" gate="G$1" x="340.36" y="68.58" smashed="yes">
<attribute name="VALUE" x="336.55" y="58.674" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="336.55" y="76.708" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="U$1" gate="G$1" x="124.46" y="177.8" smashed="yes">
<attribute name="NAME" x="118.11" y="173.99" size="1.27" layer="95"/>
<attribute name="VALUE" x="125.73" y="173.99" size="1.27" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="22.86" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="21.3614" y="107.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.162" y="107.95" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="29.718" y="107.95" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="27.94" y="107.95" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="22.86" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="21.3614" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.162" y="90.17" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="29.718" y="90.17" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="27.94" y="90.17" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="P+7" gate="1" x="22.86" y="124.46" smashed="yes">
<attribute name="VALUE" x="22.86" y="127" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="22.86" y="76.2" smashed="yes">
<attribute name="VALUE" x="20.32" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="7.62" y="99.06" smashed="yes">
<attribute name="NAME" x="11.176" y="97.028" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.335" y="97.028" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="7.62" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="85.09" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="14.478" y="85.09" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="12.7" y="85.09" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="7.62" y="76.2" smashed="yes">
<attribute name="VALUE" x="5.08" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="J9" gate="G$1" x="254" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="251.46" y="228.6" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="257.81" y="228.6" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J10" gate="G$1" x="149.86" y="180.34" smashed="yes">
<attribute name="NAME" x="147.32" y="182.88" size="1.778" layer="95"/>
<attribute name="VALUE" x="147.32" y="176.53" size="1.778" layer="96"/>
</instance>
<instance part="J11" gate="G$1" x="20.32" y="152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="22.86" y="149.86" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="22.86" y="156.21" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J12" gate="G$1" x="88.9" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="91.44" y="78.74" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="91.44" y="85.09" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="1" x="5.08" y="152.4" smashed="yes" rot="R90">
<attribute name="VALUE" x="2.54" y="152.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="322.58" y="68.58" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="71.12" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND9" gate="1" x="358.14" y="68.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="360.68" y="66.04" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="358.14" y="63.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="360.68" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="P+9" gate="1" x="322.58" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="320.04" y="73.66" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J13" gate="G$1" x="307.34" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="309.88" y="60.96" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="309.88" y="67.31" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+10" gate="1" x="289.56" y="63.5" smashed="yes" rot="R90">
<attribute name="VALUE" x="287.02" y="63.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U2" gate="A" x="261.62" y="83.82" smashed="yes">
<attribute name="NAME" x="248.92" y="120.38" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="248.92" y="44.26" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="C5" gate="G$1" x="106.68" y="175.26" smashed="yes">
<attribute name="NAME" x="108.204" y="178.181" size="1.778" layer="95"/>
<attribute name="VALUE" x="108.204" y="173.101" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="108.204" y="171.196" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="108.204" y="169.418" size="1.27" layer="97"/>
<attribute name="TYPE" x="108.204" y="167.64" size="1.27" layer="97"/>
</instance>
<instance part="GND10" gate="1" x="124.46" y="165.1" smashed="yes">
<attribute name="VALUE" x="121.92" y="162.56" size="1.778" layer="96"/>
</instance>
<instance part="P+11" gate="1" x="160.02" y="180.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="162.56" y="180.34" size="1.778" layer="96"/>
</instance>
<instance part="P+12" gate="1" x="96.52" y="180.34" smashed="yes" rot="R90">
<attribute name="VALUE" x="93.98" y="180.34" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C6" gate="G$1" x="139.7" y="175.26" smashed="yes">
<attribute name="NAME" x="141.224" y="178.181" size="1.778" layer="95"/>
<attribute name="VALUE" x="141.224" y="173.101" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="141.224" y="171.196" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="141.224" y="169.418" size="1.27" layer="97"/>
<attribute name="TYPE" x="141.224" y="167.64" size="1.27" layer="97"/>
</instance>
<instance part="P+13" gate="1" x="254" y="246.38" smashed="yes">
<attribute name="VALUE" x="254" y="248.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND11" gate="1" x="284.48" y="185.42" smashed="yes" rot="R90">
<attribute name="VALUE" x="287.02" y="182.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V2" gate="G$1" x="231.14" y="134.62" smashed="yes" rot="R90">
<attribute name="VALUE" x="228.6" y="134.62" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="236.22" y="127" smashed="yes">
<attribute name="VALUE" x="233.68" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="287.02" y="45.72" smashed="yes">
<attribute name="VALUE" x="284.48" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="J14" gate="G$1" x="48.26" y="40.64" smashed="yes" rot="R90">
<attribute name="VALUE" x="53.086" y="38.1" size="1.778" layer="96" font="vector" rot="R90"/>
<attribute name="NAME" x="42.672" y="38.1" size="1.778" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="223.52" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="77.2414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="227.33" y="76.962" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="227.33" y="80.518" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="227.33" y="78.74" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="R7" gate="G$1" x="218.44" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="67.0814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="222.25" y="64.262" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="222.25" y="75.438" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="222.25" y="73.66" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="208.28" y="63.5" smashed="yes">
<attribute name="VALUE" x="205.74" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="P+14" gate="1" x="337.82" y="116.84" smashed="yes" rot="R270">
<attribute name="VALUE" x="340.36" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="+3V3" gate="G$1" x="304.8" y="114.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="307.34" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="284.48" y="109.22" smashed="yes">
<attribute name="NAME" x="286.004" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="286.004" y="107.061" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="286.004" y="105.156" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="286.004" y="103.378" size="1.27" layer="97"/>
<attribute name="TYPE" x="286.004" y="101.6" size="1.27" layer="97"/>
</instance>
<instance part="C8" gate="G$1" x="294.64" y="109.22" smashed="yes">
<attribute name="NAME" x="296.164" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="296.164" y="107.061" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="296.164" y="105.156" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="296.164" y="103.378" size="1.27" layer="97"/>
<attribute name="TYPE" x="296.164" y="101.6" size="1.27" layer="97"/>
</instance>
<instance part="C9" gate="G$1" x="317.5" y="111.76" smashed="yes">
<attribute name="NAME" x="319.024" y="114.681" size="1.778" layer="95"/>
<attribute name="VALUE" x="319.024" y="109.601" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="319.024" y="107.696" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="319.024" y="105.918" size="1.27" layer="97"/>
<attribute name="TYPE" x="319.024" y="104.14" size="1.27" layer="97"/>
</instance>
<instance part="C10" gate="G$1" x="327.66" y="111.76" smashed="yes">
<attribute name="NAME" x="329.184" y="114.681" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="109.601" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="107.696" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="105.918" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="104.14" size="1.27" layer="97"/>
</instance>
<instance part="GND15" gate="1" x="284.48" y="99.06" smashed="yes">
<attribute name="VALUE" x="281.94" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="317.5" y="101.6" smashed="yes">
<attribute name="VALUE" x="314.96" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="218.44" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="216.408" y="42.164" size="1.778" layer="95"/>
<attribute name="VALUE" x="216.408" y="40.005" size="1.778" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="208.28" y="45.72" smashed="yes">
<attribute name="NAME" x="204.47" y="47.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="42.418" size="1.778" layer="96"/>
<attribute name="PRECISION" x="204.47" y="38.862" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="204.47" y="40.64" size="1.27" layer="97"/>
</instance>
<instance part="+3V4" gate="G$1" x="233.68" y="45.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="236.22" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="J15" gate="G$1" x="182.88" y="43.18" smashed="yes">
<attribute name="VALUE" x="180.34" y="38.354" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="180.34" y="48.768" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND17" gate="1" x="198.12" y="40.64" smashed="yes">
<attribute name="VALUE" x="195.58" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="218.44" y="33.02" smashed="yes" rot="R270">
<attribute name="NAME" x="216.408" y="29.464" size="1.778" layer="95"/>
<attribute name="VALUE" x="216.408" y="27.305" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="208.28" y="33.02" smashed="yes">
<attribute name="NAME" x="204.47" y="34.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="29.718" size="1.778" layer="96"/>
<attribute name="PRECISION" x="204.47" y="26.162" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="204.47" y="27.94" size="1.27" layer="97"/>
</instance>
<instance part="+3V5" gate="G$1" x="233.68" y="33.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="236.22" y="33.02" size="1.778" layer="96"/>
</instance>
<instance part="J16" gate="G$1" x="182.88" y="30.48" smashed="yes">
<attribute name="VALUE" x="180.34" y="25.654" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="180.34" y="36.068" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND18" gate="1" x="198.12" y="27.94" smashed="yes">
<attribute name="VALUE" x="195.58" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="218.44" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="216.408" y="16.764" size="1.778" layer="95"/>
<attribute name="VALUE" x="216.408" y="14.605" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="208.28" y="20.32" smashed="yes">
<attribute name="NAME" x="204.47" y="21.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="17.018" size="1.778" layer="96"/>
<attribute name="PRECISION" x="204.47" y="13.462" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="204.47" y="15.24" size="1.27" layer="97"/>
</instance>
<instance part="+3V6" gate="G$1" x="233.68" y="20.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="236.22" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="J17" gate="G$1" x="182.88" y="17.78" smashed="yes">
<attribute name="VALUE" x="180.34" y="12.954" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="180.34" y="23.368" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND19" gate="1" x="198.12" y="15.24" smashed="yes">
<attribute name="VALUE" x="195.58" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="218.44" y="7.62" smashed="yes" rot="R270">
<attribute name="NAME" x="216.408" y="4.064" size="1.778" layer="95"/>
<attribute name="VALUE" x="216.408" y="1.905" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="208.28" y="7.62" smashed="yes">
<attribute name="NAME" x="204.47" y="9.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="4.318" size="1.778" layer="96"/>
<attribute name="PRECISION" x="204.47" y="0.762" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="204.47" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="+3V7" gate="G$1" x="233.68" y="7.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="236.22" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="J18" gate="G$1" x="182.88" y="5.08" smashed="yes">
<attribute name="VALUE" x="180.34" y="0.254" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="180.34" y="10.668" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND20" gate="1" x="198.12" y="2.54" smashed="yes">
<attribute name="VALUE" x="195.58" y="0" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="53.34" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="50.8" y="123.9774" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="50.8" y="126.7714" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="Z" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="Z"/>
<wire x1="101.6" y1="119.38" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<label x="91.44" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="18"/>
<wire x1="63.5" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<label x="68.58" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="Y"/>
<wire x1="101.6" y1="121.92" x2="91.44" y2="121.92" width="0.1524" layer="91"/>
<label x="91.44" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="17"/>
<wire x1="48.26" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<label x="40.64" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B"/>
<wire x1="101.6" y1="127" x2="91.44" y2="127" width="0.1524" layer="91"/>
<label x="91.44" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="13"/>
<wire x1="48.26" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<label x="40.64" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A"/>
<wire x1="101.6" y1="129.54" x2="91.44" y2="129.54" width="0.1524" layer="91"/>
<label x="91.44" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="14"/>
<wire x1="63.5" y1="160.02" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<label x="68.58" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="15"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="48.26" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="157.48" x2="40.64" y2="157.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
<pinref part="J2" gate="G$1" pin="29"/>
<wire x1="45.72" y1="149.86" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="147.32" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<wire x1="45.72" y1="144.78" x2="45.72" y2="142.24" width="0.1524" layer="91"/>
<wire x1="45.72" y1="142.24" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<wire x1="45.72" y1="139.7" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="27"/>
<wire x1="48.26" y1="142.24" x2="45.72" y2="142.24" width="0.1524" layer="91"/>
<junction x="45.72" y="142.24"/>
<pinref part="J2" gate="G$1" pin="25"/>
<wire x1="48.26" y1="144.78" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<junction x="45.72" y="144.78"/>
<pinref part="J2" gate="G$1" pin="23"/>
<wire x1="48.26" y1="147.32" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<junction x="45.72" y="147.32"/>
<pinref part="J2" gate="G$1" pin="21"/>
<wire x1="48.26" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<junction x="45.72" y="149.86"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="J2" gate="G$1" pin="16"/>
<wire x1="71.12" y1="157.48" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="66.04" y1="157.48" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="175.26" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="175.26" x2="66.04" y2="172.72" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="10"/>
<wire x1="66.04" y1="172.72" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="170.18" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="167.64" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="165.1" x2="63.5" y2="165.1" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="63.5" y1="167.64" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<junction x="66.04" y="167.64"/>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="63.5" y1="170.18" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="66.04" y="170.18"/>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="63.5" y1="172.72" x2="66.04" y2="172.72" width="0.1524" layer="91"/>
<junction x="66.04" y="172.72"/>
<wire x1="66.04" y1="165.1" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<junction x="66.04" y="165.1"/>
<junction x="66.04" y="157.48"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="101.6" y1="66.04" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="96.52" y1="66.04" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<wire x1="60.96" y1="93.98" x2="60.96" y2="81.28" width="0.1524" layer="91"/>
<wire x1="60.96" y1="81.28" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<wire x1="60.96" y1="68.58" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
<wire x1="60.96" y1="58.42" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<wire x1="60.96" y1="53.34" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="55.88" y1="106.68" x2="60.96" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="55.88" y1="116.84" x2="60.96" y2="116.84" width="0.1524" layer="91"/>
<wire x1="60.96" y1="116.84" x2="60.96" y2="106.68" width="0.1524" layer="91"/>
<junction x="60.96" y="106.68"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="55.88" y1="58.42" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
<junction x="60.96" y="58.42"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="55.88" y1="68.58" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<junction x="60.96" y="68.58"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="55.88" y1="81.28" x2="60.96" y2="81.28" width="0.1524" layer="91"/>
<junction x="60.96" y="81.28"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="55.88" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<junction x="60.96" y="93.98"/>
<pinref part="J14" gate="G$1" pin="1"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="53.34" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<junction x="60.96" y="53.34"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="55.88" y1="124.46" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="60.96" y1="124.46" x2="60.96" y2="116.84" width="0.1524" layer="91"/>
<junction x="60.96" y="116.84"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="J7" gate="G$1" pin="PWRPAD"/>
<wire x1="137.16" y1="55.88" x2="142.24" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="22.86" y1="78.74" x2="22.86" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="7.62" y1="83.82" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="6"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="347.98" y1="68.58" x2="355.6" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="5"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="332.74" y1="68.58" x2="325.12" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="106.68" y1="172.72" x2="106.68" y2="170.18" width="0.1524" layer="91"/>
<wire x1="106.68" y1="170.18" x2="124.46" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="170.18" x2="124.46" y2="175.26" width="0.1524" layer="91"/>
<wire x1="124.46" y1="170.18" x2="124.46" y2="167.64" width="0.1524" layer="91"/>
<junction x="124.46" y="170.18"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="124.46" y1="170.18" x2="139.7" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="139.7" y1="170.18" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND_A"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="274.32" y1="185.42" x2="281.94" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="236.22" y1="129.54" x2="243.84" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="GND"/>
<wire x1="279.4" y1="50.8" x2="287.02" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="287.02" y1="50.8" x2="287.02" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="EPAD"/>
<wire x1="279.4" y1="53.34" x2="287.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="287.02" y1="53.34" x2="287.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="287.02" y="50.8"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="208.28" y1="66.04" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="208.28" y1="68.58" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="218.44" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="73.66" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<junction x="208.28" y="68.58"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="317.5" y1="109.22" x2="327.66" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="317.5" y1="104.14" x2="317.5" y2="109.22" width="0.1524" layer="91"/>
<junction x="317.5" y="109.22"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="284.48" y1="106.68" x2="294.64" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="284.48" y1="101.6" x2="284.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="284.48" y="106.68"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="190.5" y1="43.18" x2="198.12" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J16" gate="G$1" pin="1"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="190.5" y1="30.48" x2="198.12" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J17" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="190.5" y1="17.78" x2="198.12" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J18" gate="G$1" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="190.5" y1="5.08" x2="198.12" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="P+5" gate="1" pin="+24V"/>
<pinref part="J2" gate="G$1" pin="20"/>
<wire x1="71.12" y1="152.4" x2="66.04" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="30"/>
<wire x1="66.04" y1="152.4" x2="63.5" y2="152.4" width="0.1524" layer="91"/>
<wire x1="63.5" y1="139.7" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="139.7" x2="66.04" y2="142.24" width="0.1524" layer="91"/>
<junction x="66.04" y="152.4"/>
<pinref part="J2" gate="G$1" pin="22"/>
<wire x1="66.04" y1="142.24" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<wire x1="66.04" y1="144.78" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="147.32" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<wire x1="66.04" y1="149.86" x2="66.04" y2="152.4" width="0.1524" layer="91"/>
<wire x1="63.5" y1="149.86" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<junction x="66.04" y="149.86"/>
<pinref part="J2" gate="G$1" pin="24"/>
<wire x1="63.5" y1="147.32" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<junction x="66.04" y="147.32"/>
<pinref part="J2" gate="G$1" pin="26"/>
<wire x1="63.5" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<junction x="66.04" y="144.78"/>
<pinref part="J2" gate="G$1" pin="28"/>
<wire x1="63.5" y1="142.24" x2="66.04" y2="142.24" width="0.1524" layer="91"/>
<junction x="66.04" y="142.24"/>
</segment>
<segment>
<pinref part="P+6" gate="1" pin="+24V"/>
<pinref part="J2" gate="G$1" pin="11"/>
<wire x1="40.64" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="9"/>
<wire x1="45.72" y1="162.56" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="165.1" x2="45.72" y2="165.1" width="0.1524" layer="91"/>
<wire x1="45.72" y1="165.1" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<junction x="45.72" y="162.56"/>
<pinref part="J2" gate="G$1" pin="7"/>
<wire x1="48.26" y1="167.64" x2="45.72" y2="167.64" width="0.1524" layer="91"/>
<wire x1="45.72" y1="167.64" x2="45.72" y2="165.1" width="0.1524" layer="91"/>
<junction x="45.72" y="165.1"/>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="48.26" y1="170.18" x2="45.72" y2="170.18" width="0.1524" layer="91"/>
<wire x1="45.72" y1="170.18" x2="45.72" y2="167.64" width="0.1524" layer="91"/>
<junction x="45.72" y="167.64"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="48.26" y1="172.72" x2="45.72" y2="172.72" width="0.1524" layer="91"/>
<wire x1="45.72" y1="172.72" x2="45.72" y2="170.18" width="0.1524" layer="91"/>
<junction x="45.72" y="170.18"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="175.26" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<wire x1="45.72" y1="175.26" x2="45.72" y2="172.72" width="0.1524" layer="91"/>
<junction x="45.72" y="172.72"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="106.68" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="45.72" y1="106.68" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+24V"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="116.84" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="124.46" x2="45.72" y2="127" width="0.1524" layer="91"/>
<wire x1="48.26" y1="116.84" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<junction x="45.72" y="116.84"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="93.98" x2="45.72" y2="93.98" width="0.1524" layer="91"/>
<wire x1="45.72" y1="93.98" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="45.72" y="106.68"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="93.98" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<junction x="45.72" y="93.98"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="45.72" y1="81.28" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<wire x1="45.72" y1="68.58" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<junction x="45.72" y="68.58"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="48.26" y1="81.28" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<junction x="45.72" y="81.28"/>
<pinref part="J14" gate="G$1" pin="2"/>
<wire x1="45.72" y1="58.42" x2="45.72" y2="48.26" width="0.1524" layer="91"/>
<junction x="45.72" y="58.42"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="50.8" y1="124.46" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<junction x="45.72" y="124.46"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+24V"/>
<pinref part="J8" gate="G$1" pin="PWRPAD"/>
<wire x1="137.16" y1="45.72" x2="142.24" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+7" gate="1" pin="+24V"/>
<wire x1="22.86" y1="116.84" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+24V"/>
<pinref part="J3" gate="G$1" pin="10"/>
<wire x1="355.6" y1="63.5" x2="347.98" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<pinref part="P+9" gate="1" pin="+24V"/>
<wire x1="332.74" y1="73.66" x2="325.12" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IN"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="116.84" y1="180.34" x2="106.68" y2="180.34" width="0.1524" layer="91"/>
<wire x1="106.68" y1="180.34" x2="99.06" y2="180.34" width="0.1524" layer="91"/>
<junction x="106.68" y="180.34"/>
<pinref part="P+12" gate="1" pin="+24V"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="78.74" y1="81.28" x2="83.82" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="2"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="7.62" y1="152.4" x2="15.24" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="2"/>
<pinref part="P+10" gate="1" pin="+5V"/>
<wire x1="292.1" y1="63.5" x2="302.26" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="2"/>
<pinref part="P+11" gate="1" pin="+5V"/>
<wire x1="154.94" y1="180.34" x2="157.48" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="254" y1="236.22" x2="254" y2="243.84" width="0.1524" layer="91"/>
<pinref part="P+13" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+14" gate="1" pin="+5V"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="335.28" y1="116.84" x2="327.66" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="327.66" y1="116.84" x2="317.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="327.66" y="116.84"/>
<pinref part="U2" gate="A" pin="VCC"/>
<wire x1="317.5" y1="116.84" x2="279.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="317.5" y="116.84"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<pinref part="U1" gate="G$1" pin="+3V3"/>
<wire x1="91.44" y1="73.66" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="J1" gate="G$1" pin="VCC"/>
<wire x1="233.68" y1="134.62" x2="243.84" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VL"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="279.4" y1="114.3" x2="284.48" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="284.48" y1="114.3" x2="294.64" y2="114.3" width="0.1524" layer="91"/>
<junction x="284.48" y="114.3"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="294.64" y1="114.3" x2="302.26" y2="114.3" width="0.1524" layer="91"/>
<junction x="294.64" y="114.3"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="223.52" y1="45.72" x2="231.14" y2="45.72" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="223.52" y1="33.02" x2="231.14" y2="33.02" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="223.52" y1="20.32" x2="231.14" y2="20.32" width="0.1524" layer="91"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="223.52" y1="7.62" x2="231.14" y2="7.62" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="PSU_VSENSE" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="22.86" y1="99.06" x2="22.86" y2="104.14" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="22.86" y1="104.14" x2="22.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="7.62" y1="104.14" x2="22.86" y2="104.14" width="0.1524" layer="91"/>
<junction x="22.86" y="104.14"/>
<label x="22.86" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB06/ADC1-8"/>
<wire x1="182.88" y1="83.82" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<label x="187.96" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="7.62" y1="96.52" x2="7.62" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="332.74" y1="71.12" x2="325.12" y2="71.12" width="0.1524" layer="91"/>
<label x="325.12" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="B"/>
<wire x1="243.84" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<label x="231.14" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="7"/>
<wire x1="332.74" y1="66.04" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<label x="325.12" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="Y"/>
<wire x1="243.84" y1="63.5" x2="231.14" y2="63.5" width="0.1524" layer="91"/>
<label x="231.14" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<wire x1="347.98" y1="71.12" x2="355.6" y2="71.12" width="0.1524" layer="91"/>
<label x="350.52" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="A"/>
<wire x1="243.84" y1="93.98" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
<label x="231.14" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="Z2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="8"/>
<wire x1="347.98" y1="66.04" x2="355.6" y2="66.04" width="0.1524" layer="91"/>
<label x="350.52" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="Z"/>
<wire x1="243.84" y1="58.42" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<label x="231.14" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="OUT"/>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="132.08" y1="180.34" x2="139.7" y2="180.34" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="139.7" y1="180.34" x2="144.78" y2="180.34" width="0.1524" layer="91"/>
<junction x="139.7" y="180.34"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="5V0B"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="254" y1="220.98" x2="254" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RPI_TX" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="TXD"/>
<wire x1="274.32" y1="193.04" x2="289.56" y2="193.04" width="0.1524" layer="91"/>
<label x="279.4" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA20/TC7-0/SER5-2/SER3-2"/>
<wire x1="182.88" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
<label x="187.96" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="RPI_RX" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="RXD"/>
<wire x1="274.32" y1="190.5" x2="289.56" y2="190.5" width="0.1524" layer="91"/>
<label x="279.4" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA22/TC4-0/SER5-1/SER3-0"/>
<wire x1="182.88" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<label x="187.96" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="DISP_SCL" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SCL"/>
<wire x1="243.84" y1="139.7" x2="231.14" y2="139.7" width="0.1524" layer="91"/>
<label x="231.14" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB13/TC4-1/SER4-1"/>
<wire x1="182.88" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<label x="187.96" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="DISP_SDA" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SDA"/>
<wire x1="243.84" y1="144.78" x2="231.14" y2="144.78" width="0.1524" layer="91"/>
<label x="231.14" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB12/TC4-0/SER4-0"/>
<wire x1="182.88" y1="73.66" x2="198.12" y2="73.66" width="0.1524" layer="91"/>
<label x="187.96" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="TERM100"/>
<wire x1="223.52" y1="68.58" x2="243.84" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="SRL"/>
<wire x1="228.6" y1="73.66" x2="243.84" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="2-0-PA09" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA09/ADC0-9/ADC1-3/TC0-1/TCC0-1/SER0-1/SER2-0"/>
<wire x1="182.88" y1="111.76" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<label x="185.42" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="DI"/>
<wire x1="243.84" y1="109.22" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<label x="231.14" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="2-2-PA10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10/ADC0-10/TC1-0/TCC0-2/SER0-2/SER2-2"/>
<wire x1="182.88" y1="109.22" x2="198.12" y2="109.22" width="0.1524" layer="91"/>
<label x="185.42" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="RO"/>
<wire x1="243.84" y1="78.74" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
<label x="231.14" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="RE2" class="0">
<segment>
<pinref part="U2" gate="A" pin="!RE"/>
<wire x1="243.84" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
<label x="231.14" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB07/AD1-9"/>
<wire x1="182.88" y1="81.28" x2="198.12" y2="81.28" width="0.1524" layer="91"/>
<label x="187.96" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="TERM2" class="0">
<segment>
<pinref part="U2" gate="A" pin="!TERM"/>
<wire x1="243.84" y1="99.06" x2="231.14" y2="99.06" width="0.1524" layer="91"/>
<label x="231.14" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA11/ADC0-11/TC1-1/TCC0-3/SER0-3/SER2-3"/>
<wire x1="182.88" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<label x="185.42" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="DE2" class="0">
<segment>
<pinref part="U2" gate="A" pin="DE"/>
<wire x1="243.84" y1="83.82" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<label x="231.14" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1"/>
<wire x1="182.88" y1="114.3" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<label x="185.42" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA12" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA12/TC2-0/TCC0-6/SER2-0/SER4-1"/>
<wire x1="182.88" y1="104.14" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
<label x="187.96" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="203.2" y1="33.02" x2="190.5" y2="33.02" width="0.1524" layer="91"/>
<label x="190.5" y="33.02" size="1.778" layer="95"/>
<pinref part="J16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PA13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA13/TC2-1/TCC0-7/SER2-1/SER4-0"/>
<wire x1="182.88" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<label x="187.96" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="203.2" y1="45.72" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<label x="190.5" y="45.72" size="1.778" layer="95"/>
<pinref part="J15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PB14" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB14/TC5-0/SER4-2"/>
<wire x1="182.88" y1="68.58" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<label x="187.96" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="203.2" y1="7.62" x2="190.5" y2="7.62" width="0.1524" layer="91"/>
<label x="190.5" y="7.62" size="1.778" layer="95"/>
<pinref part="J18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PB15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB15/TC5-1/SER4-3"/>
<wire x1="182.88" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<label x="187.96" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="203.2" y1="20.32" x2="190.5" y2="20.32" width="0.1524" layer="91"/>
<label x="190.5" y="20.32" size="1.778" layer="95"/>
<pinref part="J17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="213.36" y1="45.72" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="213.36" y1="33.02" x2="215.9" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="213.36" y1="20.32" x2="215.9" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="213.36" y1="7.62" x2="215.9" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="1"/>
<wire x1="312.42" y1="63.5" x2="312.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="312.42" y1="78.74" x2="355.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="355.6" y1="78.74" x2="355.6" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="355.6" y1="73.66" x2="347.98" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="9"/>
<wire x1="332.74" y1="63.5" x2="312.42" y2="63.5" width="0.1524" layer="91"/>
<junction x="312.42" y="63.5"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="19"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="48.26" y1="152.4" x2="27.94" y2="152.4" width="0.1524" layer="91"/>
<wire x1="27.94" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<wire x1="27.94" y1="152.4" x2="27.94" y2="182.88" width="0.1524" layer="91"/>
<wire x1="27.94" y1="182.88" x2="71.12" y2="182.88" width="0.1524" layer="91"/>
<junction x="27.94" y="152.4"/>
<pinref part="J2" gate="G$1" pin="12"/>
<wire x1="63.5" y1="162.56" x2="71.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="182.88" x2="71.12" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="+5V"/>
<wire x1="93.98" y1="81.28" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
