## PSU Baseboard / Bus Head

This is a circuit & mount for a small desktop DC PSU and embedded system bus-head. Documentation is sparse. 

![routed](log/2020-08-03_routed.png)
![schem](log/2020-08-03_schematic.png)

## TODO

- add CAD for PSU mount here,
- add image
- revision circuit for:
    - solder jumpers default shut for 5v usb-to-bus 
    - clear LED indication of 24v presence, 5v presence on bus 

### What's Up:
- uses [the module](https://gitlab.cba.mit.edu/jakeread/ucbus-module)
- breaks out 24V 350W & UCBus on one 2x15 IDC
- second 24V 40W UCBus channel available
- RPI mount available
    - RPI UART lines plumbed
    - 5V 2A regulator footprint to power RPI from 24V PSU
    - I2C Display Module footprint available 
    - BFCs & Filtering available 

### BOM

Coming... 

| Part | PN | 
| --- | --- |
| 15x2 IDC Plug | 609-5106-ND | 
| 15x2 IDC Socket | 609-3475-ND |
| 30 Position Ribbon Cable | 3M157924-50-ND | 
| Spade | WM19646-ND | 