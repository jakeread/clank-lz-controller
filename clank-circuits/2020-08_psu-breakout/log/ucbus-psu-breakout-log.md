## 2020 08 01

For that power spike, the transient frequency is actually quite high... 500ns 'period', not 15khz: 2mhz. To make a true filter, I should potentially add an R to my C, making a low pass filter. This could pair well with the bleed: if I do a 10k resistor here, I draw 2mA through the 10k, and should use an 80pf (so 100pf) cap to bypass. I'll throw 4 0805 footprints down here, and will start with 10k and 100pf, see how that fares. 

OK, through most of the setup. 

- pick sercom / DE / RE / TERM etc for RS485 bonus 
- pick sercom for i2c display 
- pick sercom for rpi serial 
- label cut-jumpers, config auto 

OK, done. 