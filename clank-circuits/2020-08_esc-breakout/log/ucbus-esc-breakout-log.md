## 2020 08 24

I've done the new motor mount and tested the motor. It does rip up to 46kRPM, which is oodles, and unfortunately also makes this thing loud AF. Earplugs level. This means that I should certainly aim for some kind of speed control... That makes two problems: the code and the circuit, which is OK, but more difficult is figuring how to mount an encoder to this thing. There is handily an M3 screw into the bottom of the motor, so I can perhaps get away with a magnet mounted into a small cup there, but 45k is even beyond what the AS5047P will read... I could look at pulse encoders. 

The CUI AMT-102-V encoder is what ODrive uses, it maxes at 15kRPM. Really, speed control just wants a tick on a fast pulse, so I could try to spec a hall sensor and just catch the edge. To figure how often that would come, if I aim at 60kRPM that's 1kHz rotation (wow) and there are 14 poles, so ostensibly I am catching a 14kHz tick, which seems totally reasonable. I would pair that with a timer to drop intervals into a ringbuffer on each edge and filter that to get a speed reading, closing the loop by outputting different PWM values. 

If that all fails, we can always just set small feeds and lower PWM values to minimize the speed, but I'll bet it's even loud AF around 20kRPM. I don't know how to fix that... could experiment with some little flexural elements in the motor mount to transmit less vibration to the rest of the machine. 

So... I guess it would be prudent of me to wire up some hall sensors... if I have them around... and see if I can position them around the can to get that edge. 

I'll use this FR4 also to mount the motor, and inteface it to the machine. I can try to add some rubber damping between both as well, so I'll try to find some McMaster grommets for this.

Most of the hall sensors I can find are not digital output, are analog. However, they swing -ve to +ve. I need to make sure I get high enough bandwidth, OK, and I want the right mV/mT spec. 

To figure that out, I found [this nice field strength calculator](https://www.kjmagnetics.com/fieldcalculator.asp) that tells me (roughly) I can hit about 90 mT at 2.5mm away from the neodymium (sp?) in the motor rotor. Since this will swop direction (90 -> -90) during the pole transition, I want to cover about 3.3V with about 180mT, or land around 18mV/mT. 

These saturate, so if I do more (I found digikey PN EQ731L-ND with 65 mV/mT), I'll be able to operate it as a digital switch. However, it would probably benefit me to have a real digital switch... and the best would be to have one footprint for either of these. That's just another digikey category, der. 

Alright, that's PN 480-5195-ND, same pinout/package as the analog thing above. I'll order both just in case. 

So... to the circuit. This can be simple. To figure the spacing for halls ... I need these to be in-line with stator elements, of which this motor has 12: nice 30 degree increments. I want to check the motor if they're aligned in the same way with the mount on each item, but I sort of doubt that they are. Yeah, different. I could introduce some tuning in the mount, but, like, I just want to measure speed. 

OK, done. 

![schem](2020-08-25_halls-schematic.png)
![board](2020-08-25_halls-routed.png)

## 2020 08 01

Each line can do 1.4A, have 14.6 max from the PSU, so call it 10 additional GND and 10 additional V+ on the board, so on top of the current 10 pin, I just need 15x2. 

I should check that these are 'commonly' available IDC sizes... seems likely that 16x2 would be favoured or something. Ah 15x2 is fine. 

| Part | PN | 
| --- | --- |
| 15x2 IDC Plug | 609-5106-ND | 
| 15x2 IDC Socket | 609-3475-ND |
| 30 Position Ribbon Cable | 3M157924-50-ND | 

OK then this is simple... I should also include a header for SPI encoder reading. I'll line that up with the stepper's SPI lines to its on board encoder. 