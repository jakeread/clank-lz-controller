/*
drivers/servo_pwm.h

output servo-type (20ms period, 1-2ms duty cycle) with TC on D51

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef SERVO_PWM_H_
#define SERVO_PWM_H_

#include <Arduino.h>

class Servo_PWM {
    private:
        static Servo_PWM* instance;
    public:
        Servo_PWM();
        static Servo_PWM* getInstance(void);
        void init(void);
        void setDuty(float duty); // 0-1 
        void onPwmTick(void); 
};

extern Servo_PWM* servo_pwm;

#endif 