/*
drivers/pulse_counter.cpp

count input width: developed to measure RPM of rotor bell past hall sensor

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "pulse_counter.h"
#include "../utils/clocks_d51_module.h"
#include "indicators.h"
#include "peripheral_nums.h"

Pulse_Counter* Pulse_Counter::instance = 0;

Pulse_Counter* Pulse_Counter::getInstance(void){
    if(instance == 0){
        instance = new Pulse_Counter();
    }
    return instance;
}

Pulse_Counter* pulse_counter = Pulse_Counter::getInstance();

Pulse_Counter::Pulse_Counter(){}

// ok, currently setup with push/pull to input on PB14, which has TC4-0 on Peripheral E

#define PULSE_PORT PORT->Group[1] 
#define PULSE_PIN 12
#define PULSE_PIN_BM (uint32_t)(1 << PULSE_PIN)

void Pulse_Counter::init(void){
    // setup PB14 to this peripheral 
    PULSE_PORT.DIRCLR.reg = PULSE_PIN_BM;   // not-output 
    PULSE_PORT.PINCFG[PULSE_PIN].bit.PMUXEN = 1; // allow peripheral mux 
    if(PULSE_PIN % 2){
        PULSE_PORT.PMUX[PULSE_PIN >> 1].reg |= PORT_PMUX_PMUXO(PERIPHERAL_E);
    } else {
        PULSE_PORT.PMUX[PULSE_PIN >> 1].reg |= PORT_PMUX_PMUXE(PERIPHERAL_E);
    }
    // setup the xtal, will use to send a clock to this timer 
    d51_clock_boss->setup_16mhz_xtal();
    // disable 
    TC4->COUNT16.CTRLA.bit.ENABLE = 0; // disable 
    // TC4->COUNT16.CTRLA.bit.SWRST = 1; // reset (would properly do this, but works without, doesn't work with)
    // unmask clocks 
    MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC4;
    // send a clock to the ch 
    GCLK->PCHCTRL[TC4_GCLK_ID].reg = GCLK_PCHCTRL_CHEN 
        | GCLK_PCHCTRL_GEN(d51_clock_boss->mhz_xtal_gclk_num);
    // setup timer 
    TC4->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 // 16 bit timer 
        | TC_CTRLA_PRESCSYNC_PRESC  // reload / reset on prescaler 
        | TC_CTRLA_PRESCALER_DIV4   // div the input clock (CALC TPS WITH THIS)
        | TC_CTRLA_CAPTEN0          // enable for capture option on ch0 
        | TC_CTRLA_COPEN0;          // capture on pin 
    // capture event defaults to the rising edge, that's OK. 
    // we need to get a capture and overflow interrupt, 
    TC4->COUNT16.INTENSET.bit.MC0 = 1;
    TC4->COUNT16.INTENSET.bit.OVF = 1;
    // now enable it, 
    while(TC4->COUNT16.SYNCBUSY.bit.ENABLE);
    TC4->COUNT16.CTRLA.bit.ENABLE = 1;
    // enable the IRQ
    NVIC_EnableIRQ(TC4_IRQn);
}

void TC4_Handler(void){
    DEBUG2PIN_TOGGLE;
    if(TC4->COUNT16.INTFLAG.bit.MC0){
        uint16_t width;
        pulse_counter->_lastWasOVF ? width = 65534 : width = TC4->COUNT16.CC[0].reg;
        pulse_counter->_lastWasOVF = false;
        // clear, stash, and reset 
        TC4->COUNT16.INTFLAG.bit.MC0 = 1; // clear 
        pulse_counter->addPulse(width); // stash 
        TC4->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_RETRIGGER; // restart (?) 
        //DEBUG1PIN_TOGGLE;
    }
    if(TC4->COUNT16.INTFLAG.bit.OVF){
        // clear, stash, reset is already happening 
        pulse_counter->_lastWasOVF = true;
        TC4->COUNT16.INTFLAG.bit.OVF = 1;
        pulse_counter->addPulse(65534); // the long nap 
        //DEBUG2PIN_TOGGLE;
    }
}

void Pulse_Counter::addPulse(uint16_t width){
    _widths[_wh] = width;
    _wh ++; 
    if(_wh >= PULSE_WIDTHS_COUNT){
        _wh = 0;
    }
}

float Pulse_Counter::getAverageWidth(void){
    float sum = 0;
    NVIC_DisableIRQ(TC4_IRQn);
    for(uint8_t i = 0; i < PULSE_WIDTHS_COUNT; i ++){
        sum += _widths[i];
    }
    NVIC_EnableIRQ(TC4_IRQn);
    return sum / (float)PULSE_WIDTHS_COUNT;
}

float Pulse_Counter::getTicksPerSecond(void){
    return PULSE_TICKS_PER_SECOND;
}