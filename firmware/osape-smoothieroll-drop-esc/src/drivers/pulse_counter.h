/*
drivers/pulse_counter.h

count input width: developed to measure RPM of rotor bell past hall sensor

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef PULSE_COUNTER_H_
#define PULSE_COUNTER_H_

#include <Arduino.h>

#define PULSE_WIDTHS_COUNT 16
#define PULSE_TICKS_PER_SECOND 4000000 // DIV256: 62500, DIV4: 4000000

class Pulse_Counter {
    private:
        static Pulse_Counter* instance;
        volatile uint16_t _widths[PULSE_WIDTHS_COUNT];
        volatile uint8_t _wh = 0; // width head (write to)
    public:
        Pulse_Counter();
        static Pulse_Counter* getInstance(void);
        void init(void);
        volatile boolean _lastWasOVF = true;
        void addPulse(uint16_t width);
        float getAverageWidth(void);
        float getTicksPerSecond(void);
};

extern Pulse_Counter* pulse_counter;

#endif