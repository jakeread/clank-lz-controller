#include <Arduino.h>

#include "drivers/indicators.h"
#include "utils/cobs.h"
#include "osap/osap.h"

OSAP* osap = new OSAP("spindle esc drop");

#include "drivers/ucbus_drop.h"

#include "utils/clocks_d51_module.h"

#include "drivers/servo_pwm.h"

union chunk_float32 {
  uint8_t bytes[4];
  float f;
};

union chunk_float64 {
  uint8_t bytes[8];
  double f;
};

#define BUS_DROP 0

void setup() {
  ERRLIGHT_SETUP;
  CLKLIGHT_SETUP;
  DEBUG1PIN_SETUP;
  DEBUG2PIN_SETUP;
  // osap
  osap->description = "remote esc drop";
  // bus 
  ucBusDrop->init(false, BUS_DROP);
  // pwm out 
  servo_pwm->init();
  servo_pwm->setDuty(0.0F);
}

uint8_t bChPck[64];
uint16_t lpCnt = 0;

// 0.1 -> nok RPM
// 0.15 -> 2.5k RPM ~ unstable stops 
// 0.2 -> 6k RPM 
// 0.3 -> 12k RPM 6k comfortable, low harmonic 
// 0.4 -> 18k RPM 6k p loud 
// 0.5 -> 23k RPM 5k loud AF 
// 0.6 -> don't do this

void loop() {
  /*
  if(millis() > 5000 && run_pwm){
    servo_pwm->setDuty(0.3F);
    run_pwm = false;
  }
  */
  osap->loop();
  // do packet B grab / handle 
  if(ucBusDrop->ctr_b()){
    uint16_t len = ucBusDrop->read_b(bChPck);
    uint16_t ptr = 0;
    switch(bChPck[ptr]){
      case AK_SETCURRENT: 
        break;
      case AK_SETPOS: 
        break;
      case AK_SETRPM: {
        ptr ++;
        chunk_float32 rpm_c; // this is actually just the 0-1 value, direct, the mapping from RPM -> PWM is done in JS 
        rpm_c.bytes[0] = bChPck[ptr ++];
        rpm_c.bytes[1] = bChPck[ptr ++];
        rpm_c.bytes[2] = bChPck[ptr ++];
        rpm_c.bytes[3] = bChPck[ptr ++];
        servo_pwm->setDuty(rpm_c.f);
        break;
      }
      default:
        // noop,
        break;
    }
  }
  // do periodic hello-to-above 
  /*
  lpCnt ++;
  if(lpCnt > 10000){
    lpCnt = 0;
    float widths = pulse_counter->getAverageWidth();
    float tps = pulse_counter->getTicksPerSecond();
    // capture on rising edges, 14 poles, 7 edges per revolution,
    float rpm = (tps * 60) / (widths * 7 + 1);
    sysError("RPM: " + String(rpm, 6));
  }
  */
} // end loop 

// the following is leftover from an attempt to speed control this thing, might revisit... 

volatile float rpm_target = 10000.0F;
volatile float p_term = -0.002F;
volatile float pid_out = 0.0F;

volatile float err = 0.0F;
volatile float widths = 0.0F;
volatile float tps = 0.0F;
volatile float rpm = 0.0F;

void Servo_PWM::onPwmTick(void){
  /*
  //DEBUG2PIN_ON;
  widths = pulse_counter->getAverageWidth();
  // avoid div / low
  if(widths < 100) widths = 100;
  tps = pulse_counter->getTicksPerSecond();
  rpm = (tps * 60) / (widths * 7);
  // presuming full width is 0-60k rpm, and outputs 0-1, 
  // I want to scale RPM err into -1 -> 1 domain, so rpm from 0-1
  // this should help (?) with numerical stability, not multiplying 60k w/ 0.00002 p term 
  rpm = rpm / 60000;
  err = rpm - (rpm_target / 60000); // err could span -60k -> 60k 
  pid_out = err * p_term;
  // no small outputs, 
  if(pid_out < 0.2F) pid_out = 0.2F;
  if(run_pwm) servo_pwm->setDuty(pid_out);
  //DEBUG2PIN_OFF;
  */
}

void UCBus_Drop::onRxISR(void){
  // 100kHz (?) or is it 50?
}

void UCBus_Drop::onPacketARx(void){
  // not really for us, 
}

void OSAP::handleAppPacket(uint8_t *pck, uint16_t pl, uint16_t ptr, uint16_t segsize, VPort* vp, uint16_t vpi, uint8_t pwp){
  vp->clearPacket(pwp);
}
