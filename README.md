## Clank-LZ Controller

The Clank-LZ Controller is a standalone / adhoc implementation of an ongoing experiment in modular controls for CNC machines *in general*. I'll make an effort in this repo to freeze development of those controllers for this particular machine, which is meant to *work* consistently and not break on the edge branch. 

![img](images/2020-09-29_clank-lz-controller-grab.png)

### Software 

The software that runs Clank is a mixture of 'server-side' javascript (running in node.js), client-side js (in the browser) and embedded c++ in the hardware modules. 

### Circuits

Circuits that run the machine can be found [in this folder](/clank-circuits), along with their firmware.

### Firmware

Each of the circuits you received should have Adafruit's Feather M4 bootloader onboard, meaning that you can build and flash firmware (if needed) using [platformio](https://platformio.org/) or (?) the Arduino IDE.

#### Install Platformio and VSCode

[Platformio](https://platformio.org/) is an extension that can work in [VSCode](https://code.visualstudio.com/) or in [Atom](https://atom.io/) though the latter is not as well supported. To build firmware, install VSCode and then use it's internal extensions-installer to install Platformio. The softwares are well documented and are cross platform, you should be able to figure out how to do this by following the links above. 

#### Open and Build Firmware

From platformio, do `file > open folder ...` and open i.e. `<this repo>/firmware/osape-smoothieroll-drop-stepper/` - Platformio should recognize it's project metadata files in this folder. Give it a few moments to get itself sorted (VSCode and Atom are built on web technologies - they are sometimes loading / reloading big messy javascript dependencies / etc).  

You should be able to do `platformio: build` from VSCode's little internal command line - in windows I can open that command line with `ctrl + shift + p` - I think VSCode calls it a 'command pallette' or something.  

Build also works with `ctrl + alt + b`  

#### Configuring Motor Firmware

In `src/main.cpp` for the stepper motor firmware, you'll find this little snippet:

```cpp
#define BUS_DROP 1    // Z: 1, YL: 2, X: 3, YR: 4
#define AXIS_PICK 2   // Z: 2, Y: 1, X: 0
#define AXIS_INVERT false  // Z: false, YL: true, YR: false, X: false
#define SPU 3200.0F // always positive! Z: 3200, XY: 400 
#define C_SCALE 0.1F // 0-1, floating: initial holding current to motor, 0-2.5A 
#define TICKS_PER_PACKET 20.0F // always 20.0F
```

These are motor-specific settings. The selection above is for an updated Z motor with less current scaling than what was originally flashed in the kits (the C_SCALE value). You'll need to swap these around when you flash different motors. 

#### Configuring PSU Breakout (Bus Head) Firmware

In 'src/config.h' for the psu-breakout (osape-smoothieroll-head) firmware, you need to do the following:

- if the circuit module (https://gitlab.cba.mit.edu/jakeread/ucbus-module) on the psu-breakout (https://gitlab.cba.mit.edu/jakeread/ucbus-psu-breakout) **has a surface mount JTAG connctor** - leave the file as is, like this:
    - `#define IS_OG_CLANK`
- if the module **has a through-hole JTAG connector**, comment this line out
    - `//#define IS_OG_CLANK`

#### Build Errors 

Platformio does not like long project directories: if you get errors for missing files, try moving the project folder higher up the path tree. 

From Anthony's attempts to build:

> I tried this on 2 windows machines.  The first one very unhelpfully threw "Error 1" saying that folders couldn't be created and provided no other info.  The second machine explicitly said that the file path was too long.  The original file path had an extra Clank-lz-controller-master when I downloaded it so it looked like Clank-lz-controller-master/Clank-lz-controller-master/all other folders here.  I removed the top most folder to shorten the path and that fix ended up working on both machines.

#### Uploading Firmware 

Once you can build, this should be easy! Inside each board we gave you is the [UF2 Bootloader](https://learn.adafruit.com/adafruit-feather-m4-express-atsamd51/uf2-bootloader-details) via Adafruit. This means that you can just plug the board of interest in via USB (don't plug two boards on the same network in to usb power at once!), and hit `platformio: upload` with your preferred build settings (those #defines above). This is also `ctrl + alt + u` in VSCode in windows.  

You should see the build complete, and then an upload process involving looking for a port ... resetting it ... waiting for a new port ... and flashing. This can be slow (ish!) so be patient. 

### Operation 

The machine eats [gcode](https://www.reprap.org/wiki/G-code), an aweful and outdated format that we love to hate. If you want to build a different interface, the controller's internal entry point for motion commands will accept a position & rate data object.  

#### Generating GCode

[mods](http://mods.cba.mit.edu/?program=programs/machines/G-code/mill%202D%20PCB%20png)

[flatcam](http://flatcam.org/) 

**Isolation Feeds & Speeds:**
- engraving bit or 1/64" flat endmill 
- 0.003" total depth in one pass  
- 0.15" feedrate: inch/sec 
- 10k rpm 

**Cutout Feeds & Speeds:**
- 1/32" flat endmill 
- 0.070" total depth in one pass
- 0.1" inch / sec 
- 10k rpm 

#### Homing & Running GCode 

![op](video/2020-10-01_clank-lz-operation.mp4)

#### Flattening the Bed 

Since there's no guarantee that the machine's bed will be flat w/r/t the machine's own geometry, we commonly 'face' CNC machine bed surfaces to establish a reference that's aligned with the machine's travel.  

The GCode that loads in the controller when you first start it up is a facing routine for the 1/8" end mill we've included in the Kit. To run that file:

- load the 1/8" endmill into the machine 
- home the machine's Z axis near the center of its travel 
- home the machine's XY axis in the lower left corner (all the way!) of its travel 
- run the facing gcode. this loads into the controller automatically on startup, but you can also find it [here](https://gitlab.cba.mit.edu/jakeread/clank-lz-controller/-/blob/master/tool/client/save/clank-lz-bed-face.gcode)

#### Tool Change 

![change](video/2020-10-01_clank-tool-change.mp4)

#### The Beeping

Your machine will occasionally beep at you (!) this is not my choice: the 20a 'ESC' (electronic speed controller) that turns the spindle motor around is an off-the-shelf part originally made for drones: it is doing the beeping. It beeps when it initializes, and it beeps consistently when it is sad about the way it initialized: it expects a 'zero' signal when it first recieves power, if the controller is requesting that it operate at any RPM above zero when you turn the power on, it will be upset. To fix this, turn the spindle RPM to zero and cycle power. 

#### Installation 

To install software needed to communicate with and run clank, you first need to download `node.js` [at this link](https://nodejs.org/) - get the LTS version, not the latest. 

On windows, I use git bash to do this: you can download it by reading [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). You should be able to use whatever terminal interface you are familiar with. 

Download this repository and navigate to it in your terminal. Do `cd tool` to drop into the javascript folder. Now you'll need to install a few `npm` (node's package manager) dependencies. 

`npm install express`

`npm install serialport`

`npm install ws` 

To run the same code in the browser (which uses ES6 modules) and in node (which doesn't) I run a package that sets some node flags automatically on the local side. To install that ([from here](https://github.com/standard-things/esm)), do:

`npm init esm`

This *should* work, and you should now be able to run the boot file:

`node clank` 

Your terminal should report:

```
clank-tool available on:            <your ip>:8080
clank-tool available on:            127.0.0.1:8080
```

You can navigate to the latter address (your local host) in the browser, and the controller should appear. When you plug the machine in, it should connect automatically, and you'll see something like:

`SERPORT at <name> OPEN`

!